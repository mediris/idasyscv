/*
 *jDavila
 *16/02/12
 * modificado:02/04/12
 * agregado campo aalcod
 */
function agregar()
{
	document.agregarform.auncod.value="";
	document.agregarform.aundes.value="";
	document.agregarform.aunubi.value="";
	document.agregarform.auntlf.value="";
	document.agregarform.aunsup.value="";
	document.agregarform.aalcod.value="";
	document.agregarform.aunloc.value="";
	$("#agregaraftsav").hide(1);
}

/*
 *jDavila
 *16/02/12
 */
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "unidadsolicitanteagregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
 *jDavila
 *16/02/12
 * modificado:02/04/12
 * agregado campo aalcod
 */
function editar(tecnico) {

	var param = [];
	param['auncod']=tecnico;

	ejecutasqlp("unidadsolicitanteinformacionphp.php",param);

	for(i in gdata){
		document.editarform.hauncod.value=gdata[i].AUNCOD;
		document.getElementById("wsauncod").innerHTML=gdata[i].AUNCOD;
		document.editarform.aundes.value=gdata[i].AUNDES;
		document.editarform.aunubi.value=gdata[i].AUNUBI;
		document.editarform.auntlf.value=gdata[i].AUNTLF;
		document.editarform.aunsup.value=gdata[i].AUNSUP;
		document.editarform.aalcod.value=gdata[i].AALCOD;
		document.editarform.aunloc.value=gdata[i].AUNLOC;
	};
	$("#editaraftsav").hide(1);
}

/*
 *jDavila
 *16/02/12
 */
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "unidadsolicitanteeditarvalidarphp.php",
			data: $("#editarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#editarform").hide(1000);
		$("#editaraftsav").show(1000);	
	}
}

/*
 *jDavila
 *16/02/12
 */
function eliminar(tecnico) {
	if (confirm('Seguro que desea borrar el registro ' + tecnico + '?'))
	{
		var param = [];
		param['auncod']=tecnico;
		ejecutasqld("unidadsolicitanteeliminar.php",param);
		location.reload();
	}
}


/*seccion de usuario*/
/*
 *jDavila
 *16/02/12
 */
function unisolusuarioagregar()
{
	document.unisolusuarioagregarform.auscod.value="";
	$("#unisolusuarioagregaraftsav").hide(1);
}

function unisolusuarioagregarvalidar()
{
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "unisolusuarioagregarvalidarphp.php",
        data: $("#unisolusuarioagregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#unisolusuarioagregardiv").hide(1000);
		$("#unisolusuarioagregaraftsav").show(1000);
	}
}

function unisolusuarioeliminar(usuario,unidsoli,des) 
{
	if (confirm('Seguro que desea borrar al Usuario ' + usuario + '?'))
	{
		//location.href="unisolusuarioeliminar.php?&usuario="+usuario+"&unidsoli="+unidsoli+"&des="+des;
		var param = [];
		param['usuario']=usuario;
		param['unidsoli']=unidsoli;
		param['des']=des;
		
		ejecutasqld("unisolusuarioeliminar.php",param);
		location.reload();
	}
}

/*seccion ususario*/
/*
 *jDavila
 *30/02/12
 */
function agregarusu()
{
	document.agregarform.auscod.value="";

	$("#agregaraftsav").hide(1);
}
/*
 *jDavila
 *30/02/12
 */
function agregarvalidarsus(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "unisolusuarioagregarvalidar.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}



/*
 *jDavila
 *16/02/12
 */
function eliminarsus(id, usol, usoldes) {
	if (confirm('Seguro que desea borrar el usuario ' + id + '?'))
	{
		var param = [];
		param['auscod']=id;
		param['auncod']=usol;
		param['aundes']=usoldes;
		
		ejecutasqld("unisolusuarioeliminar.php",param);
		location.reload();
	}
}