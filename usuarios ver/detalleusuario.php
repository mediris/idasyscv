<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtmll-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <link href="../css/estilos1.css" rel="stylesheet" type="text/css" />
  <script language="JavaScript" src="../javascript/javascript.js"></script>
  <script language="JavaScript" src="../javascript/jquery.js"></script>
  <script language="JavaScript" src="../javascript/jquery.li-scroller.1.0.js"></script>
  <title>GABC, Tu Administraci&oacute;n</title>
  <script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
  </script>
  <style type="text/css">
<!--
.Estilo6 {font-size: 9px}
.Estilo7 {font-size: 8px}
-->
  </style>
  </head>
<body id='index'>
<div id="principal" style="position:absolute; width:800px; height:544px; z-index:1">
	<div id="datosusuario" style="position:absolute; width:800px; height:219px; z-index:1">
		          <?php $sql="Select * from IDASYSW.mb20fp where auscod='$user' ";
				$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result)){			}?>

	  <table width="100%"  border="0" class="tablapasoscss">
	  <thead>
        <tr>
          <th colspan="4" scope="col"><h4>DATOS DEL USUARIO</h4> </th>
        </tr>
		</thead>
		<tbody>
        <tr>
          <td width="16%">C&oacute;digo:</td>
          <td width="30%"><div align="left"><?php echo odbc_result($result,1);?></div></td>
          <td width="15%">Sexo:</td>
          <td width="39%"><div align="center">
		<?php if (odbc_result($result,21)=='F') { echo '<img src="../imagenes/femenino.gif" width="20" height="20">' ;}
		else {echo '<img src="../imagenes/masculino.gif" width="20" height="20"> '; }?>
          </div></td>
        </tr>
        <tr>
          <td>Primer Nombre: </td>
          <td><div align="left"><?php echo odbc_result($result,2);?></div></td>
          <td>Segundo Nombre: </td>
          <td><div align="left"><?php echo odbc_result($result,3);?></div></td>
        </tr>
        <tr>
          <td>Primer Apellido:</td>
          <td><div align="left"><?php echo odbc_result($result,4);?></div></td>
          <td>Segundo Apellido: </td>
          <td><div align="left"><?php echo odbc_result($result,5);?></div></td>
        </tr>
        <tr>
          <td>Fecha Nacimiento: </td>
          <td><div align="left"><?php echo fecdma(odbc_result($result,6),'amd','.');?></div></td>
          <td>Fecha de Ingreso: </td>
          <td><div align="left"><?php echo fecdma(odbc_result($result,7),'amd','.');?></div></td>
        </tr>
        <tr>
          <td>Ficha de N&oacute;mina: </td>
          <td><div align="left"><?php echo odbc_result($result,8);?></div></td>
          <td>Supervisor:</td>
          <td><div align="left"><?php echo odbc_result($result,12);?></div></td>
        </tr>
        <tr>
          <td>Tel&eacute;fonos:</td>
          <td><div align="left"><?php echo odbc_result($result,9);?></div></td>
          <td>Usuario Supervisor:</td>
          <td><div align="left"><?php echo odbc_result($result,13);?></div></td>
        </tr>
        <tr>
          <td>Ultima Conexi&oacute;n: </td>
          <td><div align="left"><?php echo fecdma(odbc_result($result,18),'amd','.')." - ".odbc_result($result,19);?></div></td>
          <td>Ip:</td>
          <td><div align="left"><?php echo odbc_result($result,11);?></div></td>
        </tr>
		</tbody>
      </table>
  </div>
    <div id="opciones" style="position:absolute; width:390px; height:264px; z-index:1; left: -22px; top: 280px; overflow: auto;">
	    <h5>Opciones Autorizadas</h5>
        <table width="90%"  border="0" align="center" class="Estilo3">
     
      <tr>
        <th width="40%"><div id="menuh" class="cuerporec" align="left" >
					<ul id="ul">
					
					</ul>
		</div>
		<script language="JavaScript" type="text/JavaScript">
			buscarusuarioautl('<?php echo $user?>');
		</script>
		</th>
      </tr>

    </table>
  </div>
    <div id="sesiones" style="position:absolute; width:431px; height:109px; z-index:1; left: 369px; top: 280px; overflow: auto;">
	<h5>Sesiones Iniciadas</h5>
	<table width="90%"  border="0" align="center" class="usuariosconectados">
	<thead>
      <tr>
        <th scope="col">Inici&oacute;</th>
        <th scope="col">Finaliz&oacute;</th>
        <th scope="col">Direcci&oacute;n IP </th>
      </tr></thead>
	  	          <?php $sql="Select * from mb32fp where auscod='$user' order by assfei desc, asshoi desc";
				$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result)){			?>

	  <tbody>
      <tr>
        <td><?php echo fecdma(odbc_result($result,3),'amd','.')." - ".odbc_result($result,4);?></td>
        <td><?php echo fecdma(odbc_result($result,5),'amd','.')." - ".odbc_result($result,6);?></td>
        <td><?php echo odbc_result($result,7);?></td>
      </tr> <?php } ?>
   </tbody>
    </table>
  </div>
    <div id="historial" style="position:absolute; width:430px; height:155px; z-index:1; left: 370px; top: 389px; overflow: auto;">
	<h5>Historial</h5>
	<table width="90%"  border="0" align="center" class="usuariosconectados">
	<thead>
      <tr>
        <th width="65%" scope="col">Operaci&oacute;n</th>
        <th width="35%" scope="col">Fecha</th>
      </tr>
	  </thead>
	  	          <?php $sql="Select * from mb31fp where aauusr='$user' and aaumod='$modulo' order by aaufec desc, aauhor desc";
				$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result)){			?>

	  <tbody>
      <tr>
        <td><span class="Estilo6">
          <?php echo odbc_result($result,5);?>
        </span></td>
        <td><span class="Estilo7">
          <?php echo fecdma(odbc_result($result,7),'amd','.')." - ".odbc_result($result,8);?>
        </span></td>
      </tr> <?php } ?>
   	</tbody>
    </table>
  </div>
</div>
</body>
</html>
