<?php 
/*
 *jDavila
 *02/03/2012
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script src="../amcharts/amcharts.js" type="text/javascript"></script>         
<?php //include("configgrafica.php"); ?>
<script language="JavaScript" type="text/JavaScript">
	Shadowbox.init({overlayOpacity: "0.5"});
	
	<!-- inicio informacion de chart -->
	 		var chart;
			var chartData;
			//carga de datos
			var param = [];
			param['desde']= '<?php echo $desde; ?>';
			param['hasta']= '<?php echo $hasta; ?>';
			$.ajax({
					async: false,
					type: "POST",
					dataType: "JSON",
					url: "informaciongrafico.php",
					data: convertToJson(param),
					success: function(datos){
						//alert(datos);
						vdata = parseJSONx(datos);
						chartData = vdata;
					}	
			});	
			
			var tem = function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;
                chart.categoryField = "ARQSTS";
                chart.startDuration = 1;
                chart.plotAreaBorderColor = "#DADADA";
                chart.plotAreaBorderAlpha = 1;
				//chart.rotate = true;

                // Value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.axisAlpha = 0;
                valueAxis.gridAlpha = 0.1;
                valueAxis.position = "top";
                chart.addValueAxis(valueAxis);

                // GRAPHS
				$.ajax({
					async: false,
					type: "POST",
					dataType: "script",
					url: "configgrafica.php",
					data: convertToJson(param),
					success: function(datos){
						cdata = datos;
					}	
				});
/*
				// first graph
                var graph1 = new AmCharts.AmGraph();
                graph1.type = "column";
                graph1.title = "CMC";
                graph1.valueField = "CMC";
                //graph1.balloonText = "CMC:[[value]]";
                graph1.lineAlpha = 0;
                //graph1.fillColors = "#ADD981";
                graph1.fillAlphas = 1;
                chart.addGraph(graph1);
*/
	
                // LEGEND
                var legend = new AmCharts.AmLegend();
                chart.addLegend(legend);

                // WRITE
                chart.write("chartdiv");
            };
			
			
            AmCharts.ready(tem);
	<!-- fin informacion de chart-->
</script>
</head>
<body>
<div id="wrapper">
  <?php 
  		include("../superior.php");
  		?>
  <div id="page">
      <?php include("../validar.php");  		?>

<!--<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Archives</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
      </ul>
	    </div>-->
<!--	  <div id="content" style="background-image: url(fondoimagen.png);">-->
   	  <div id="content2">
      	<div align="center">
        <br />
      	  <h1>Estad&iacute;stica de Pedidos por Status</h1></div>
          <br />
      	<form name="form" id="form" method="get">
	        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <table>
                <tr>
                    <td>&nbsp;Desde:</td>
                    <td>&nbsp;<?php echo escribe_formulario_fecha_vacio('desde','form',$desde); ?></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;Hasta:</td>
                    <td>&nbsp;<?php echo escribe_formulario_fecha_vacio('hasta','form',$hasta); ?></td>
                    <td><a href="javascript:busqueda();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                </tr>
            </table>
        </form>
 		<div id="chartdiv" style="width: 100%; height: 500px;"></div>
	  </div><!-- end #content -->
	
		
		
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
</div>
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
