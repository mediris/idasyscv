/*
jDavila
19/03/12
*/
function agregar()
{
	document.form.aiucod.value="";
	document.form.aiudes.value="";
	document.form.aiuniv1.value="";
	document.form.aiuniv2.value="";
	document.form.aiuniv3.value="";
	document.form.aiuniv4.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
19/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "iubicacionagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
19/03/12
*/
function editar(tipo) {

	var param = [];
	param['aiucod']=tipo;
	ejecutasqlp("ubicacioninformacionphp.php",param);

	for(i in gdata)
	{
		document.form.aiucod.value=gdata[i].AIUCOD;
		document.getElementById("wsaiucod").innerHTML=gdata[i].AIUCOD;
		document.form.aiudes.value=gdata[i].AIUDES;
		document.form.aiuniv1.value=gdata[i].AIUNIV1;
		document.form.aiuniv2.value=gdata[i].AIUNIV2;
		document.form.aiuniv3.value=gdata[i].AIUNIV3;
		document.form.aiuniv4.value=gdata[i].AIUNIV4;
		validaetotalcaracteres();
	};
	$("#aftsav").hide(1);
}

/*
jDavila
19/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "iubicacioneditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
19/03/12
*/
function eliminar(tipo) {
	if (confirm('Seguro que desea borrar la ubicacion ' + tipo + '?'))
	{
		var param = [];
		param['aiucod']=tipo;
		ejecutasqld("iubicacioneliminar.php",param);
		location.reload();
	}
}
/**/
function Tecla(e, field)	{
	key = e.keyCode ? e.keyCode : e.which
	// backspace
	if (key == 8) return true
  
  	// 0-9
  	if ((key > 47 && key < 58) || (key==9)) {
	    if (field.value == "") return true
    	regexp = /[.][0-9]{3}$/;
		//alert(regexp.test(field.value));
	    return !(regexp.test(field.value))
  	}

	// ,
	if (key == 46) {
  		if (field.value == "") return false
		regexp = /^[0-9]+$/
	    return regexp.test(field.value)
  	} 
  // other key
  return false
}

function validatotalcaracteres(nivel) {
	if (document.form.aiuniv1.value=="") {niv1=0;}
	else								 {niv1=parseInt(document.form.aiuniv1.value);}
	if (document.form.aiuniv2.value=="") {niv2=0;}
	else								 {niv2=parseInt(document.form.aiuniv2.value);}
	if (document.form.aiuniv3.value=="") {niv3=0;}
	else								 {niv3=parseInt(document.form.aiuniv3.value);}
	if (document.form.aiuniv4.value=="") {niv4=0;}
	else								 {niv4=parseInt(document.form.aiuniv4.value);}

	nivs=15-(niv1+niv2+niv3+niv4);
	document.form.aiunivs.value=nivs;
}
function validaetotalcaracteres() {
	if (document.form.aiuniv1.value=="") {niv1=0;}
	else								 {niv1=parseInt(document.form.aiuniv1.value);}
	if (document.form.aiuniv2.value=="") {niv2=0;}
	else								 {niv2=parseInt(document.form.aiuniv2.value);}
	if (document.form.aiuniv3.value=="") {niv3=0;}
	else								 {niv3=parseInt(document.form.aiuniv3.value);}
	if (document.form.aiuniv4.value=="") {niv4=0;}
	else								 {niv4=parseInt(document.form.aiuniv4.value);}

	nivs=15-(niv1+niv2+niv3+niv4);
	document.form.aiunivs.value=nivs;
	
}
