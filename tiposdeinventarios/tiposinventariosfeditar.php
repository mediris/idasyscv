<?php 
/*
* jdavila 
* 02/03/2012
*/
session_start();
include("../conectar.php");
include("../JSON.php");
$aticod=$_REQUEST["tipo"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>editar tipo inventario</title>
<link href="../style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:editar('<?php echo $aticod;?>')">

<div id="aediv" class="white_content">

        <form id="form" name="form" method="post" action="" class="form">
                <table width="100%"  border="0">
              <tr>
                <th width="20%" scope="col"><label>Tipo Inventario</label></th>
                <th width="36%" id="wsaticod" align="left" scope="col"></th>
                <th colspan="2" id="erraticod" class="Estilo5" scope="col">&nbsp;<input name="haticod" id="haticod" type="hidden"></th>
              </tr>
              <tr>
                <th scope="col"><div align="left">Descripci�n</div></th>
                <th scope="col"><div align="left"><textarea name="atides" cols="50" rows="2" id="atides" ></textarea></div></th>
                <th colspan="2" id="erratides" class="Estilo5" scope="col">&nbsp;</th>
              </tr>
              <tr>
                <th scope="col"><div align="left">Niveles:</div></th>
                <th scope="col"><div align="left">
                  <input name="atiniv1" type="text" id="atiniv1" size="2" maxlength="2" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteresan(this)">
                - <input name="atiniv2" type="text" id="atiniv2" size="2" maxlength="2" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteresan(this)">
                - <input name="atiniv3" type="text" id="atiniv3" size="2" maxlength="2" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteresan(this)">
                - <input name="atiniv4" type="text" id="atiniv4" size="2" maxlength="2" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteresan(this)">
                Longitud Saldo: 
                <input name="atinivs" type="text" disabled="disabled" id="atinivs" value="15" size="2" maxlength="2">
                </div></th>
                <th colspan="2" id="erratiniv" class="Estilo5" scope="col">&nbsp;</th>
              </tr>
              <tr>
                <th width="20%" scope="col"><div align="left">Numeraci�n Autom�tica:</div></th>
                <th width="36%" align="left" scope="col">
                <label><input name="atiaut" type="radio" value="S" onChange="automatizar()">Si</label>
                <label><input type="radio" name="atiaut" value="N" checked onChange="desautomatizar()">No</label></th>
                <th colspan="2" scope="col"></th>
              </tr>
              <tr>
                <th scope="col"><div align="left">Estructura C�digo Art�culo:</div></th>
                <th scope="col"><div align="left">
                  <input name="atieca1" type="text" id="atieca1" size="2" maxlength="2" value="" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteresa(this)">
                - <input name="atieca2" type="text" id="atieca2" size="2" maxlength="2" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteresa(this)">
                - <input name="atieca3" type="text" id="atieca3" size="2" maxlength="2" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteresa(this)">
                - <input name="atieca4" type="text" id="atieca4" size="2" maxlength="2" onKeyPress="return Tecla(event, this);" onchange="javascript:validatotalcaracteresa(this)">
                Longitud Saldo: 
                <input name="atiecas" type="text" disabled="disabled" id="atiecas" value="15" size="2" maxlength="2">
                </div></th>
                <th colspan="2" id="erratieca" class="Estilo5" scope="col">&nbsp;</th>
              </tr>
              <tr>
                <th scope="col"><div align="left">Tipo de Costo </div></th>
                <th scope="col"><div align="left">
                  <select name="atitco" id="atitco" size="1">
                    <option value="1">Promedio</option>
                    <option value="2">Fijo</option>
                    <option value="3">&Uacute;ltimo Costo</option>
                  </select>
                </div></th>
                <th colspan="2" id="erratitco" class="Estilo5" scope="col">&nbsp;</th>
              </tr>
              <tr id="nivel1" style="display:none;">
                <th scope="col"><div align="left">Etiqueta 1er Nivel </div></th>
                <th scope="col"><div align="left"><input name="atid1n" id="atid1n" type="text"  size="15" maxlength="15"></div></th>
                <th colspan="2" id="erratid1n" class="Estilo5" scope="col">&nbsp;</th>
              </tr>
              <tr id="nivel2" style="display:none;">
                <th scope="col"><div align="left">Etiqueta 2do Nivel </div></th>
                <th scope="col"><div align="left"><input name="atid2n" id="atid2n" type="text"  size="15" maxlength="15"></div></th>
                <th colspan="2" id="erratid2n" class="Estilo5" scope="col">&nbsp;</th>
              </tr>
              <tr id="nivel3" style="display:none;">
                <th scope="col"><div align="left">Etiqueta 3er Nivel </div></th>
                <th scope="col"><div align="left"><input name="atid3n" id="atid3n" type="text"  size="15" maxlength="15"></div></th>
                <th colspan="2" id="erratid3n" class="Estilo5" scope="col">&nbsp;</th>
              </tr>
              <tr id="nivel4" style="display:none;">
                <th scope="col"><div align="left">Etiqueta 4to Nivel </div></th>
                <th scope="col"><div align="left"><input name="atid4n" id="atid4n" type="text"  size="15" maxlength="15"></div></th>
                <th colspan="2" id="erratid4n" class="Estilo5" scope="col">&nbsp;</th>
              </tr>
			  <tr>
                <th colspan="4" scope="col">
                	<h3>Par&aacute;metros Adicionales </h3>
                    <table width="60%"  border="0" align="center" class="Estilo3">
                    	<tr>
                        	<th scope="col"><div align="left">Seleccione el par&aacute;metro a utilizar: </div></th>
                            <th  align="left" scope="col"></th>
                        </tr>
                        <tr>
                            <th colspan="2" scope="col"vwidth="40%">
                                <div id="parama" align="left" >
                                <ul id="pa"></ul>
                                </div>
                                <script language="javascript">
									buscarparametrosa('<?php echo $aticod;?>');
								</script>
                            </th>
                         </tr>
                     </table>
            	</th>
              </tr>
              <tr>
                <th scope="col">&nbsp;</th>
                <th width="23%"  scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"  value="Salir" id="cancelar"></th>
                <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="editarvalidar()" /></th>
                <th scope="col">&nbsp;</th>
              </tr>
            </table> 
    	</form>
</div>

<div align="center" id="aftsav">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>