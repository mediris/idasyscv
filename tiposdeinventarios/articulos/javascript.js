/*
jDavila
13/03/12
*/

function agregar() {
	document.agregarform.aarcod.value="";
	document.agregarform.aardes.value="";
	document.agregarform.amccod.value="";
	document.agregarform.aarumb.value="";
	document.agregarform.aarlot.value="";
	document.agregarform.aarser.value="";
	$("#detalle").css("display", "none");
			
				
	$("#agregardiv").show(1);
	$("#agregaraftsav").hide(1);
}

/*
jDavila
14/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "articuloinagregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
jDavila
14/03/12
*/
function editar(cod) {
	var param2 = [];
	param2['aarcod']=cod;
//	param['asicod']=stipo;
	ejecutasqlp("articuloininformacionphp.php",param2);
	for(i in gdata)
	{
		document.editarform.haarcod.value=gdata[i].AARCOD;
		document.getElementById("wsaarcod").innerHTML=gdata[i].AARCOD;
		document.editarform.aardes.value=gdata[i].AARDES;		
		document.editarform.amccod.value=gdata[i].AMCCOD;
		document.editarform.stinventariob.value=gdata[i].ASICOD;
		document.editarform.stinventario.value=gdata[i].ASICOD;
		document.editarform.stinventariold.value=gdata[i].ASICOD;
		document.editarform.aarumb.value=gdata[i].AARUMB;
		if(gdata[i].AARLOT=='S') {document.editarform.aarlot[0].checked=true;}
		if(gdata[i].AARLOT=='N') {document.editarform.aarlot[1].checked=true;}
		if(gdata[i].AARSER=='S') {document.editarform.aarser[0].checked=true;}
		if(gdata[i].AARSER=='N') {document.editarform.aarser[1].checked=true;}
		cargaralmacen(document.editarform.aarumb.value, '2', document.editarform.haarcod.value);
	};
	$("#editaraftsav").hide(1);
	$("#detalle").css("display", "block");
}

/*
jDavila
07/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "articulosineditarvalidarphp.php",
			data: $("#editarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#editarform").hide(1000);
		$("#editaraftsav").show(1000);	
	}

}

/*
jDavila
07/03/12
*/
function eliminar(articulo, tipo, stipo) {
	if (confirm('Seguro que desea borrar el articulo ' + articulo + ' subtipo de inventario ' + stipo + ' perteneciente al tipo inventario ' + tipo + ' ?'))
	{
		var param = [];
		param['aarcod']=articulo;
		param['aticod']=tipo;
		param['asicod']=stipo;
		ejecutasqld("articuloineliminar.php",param);
		document.body.style.cursor="wait";
		location.reload();
	}
}
/**/

function buscanivelart(art, est){

	if(art.length>=est) {$("#detalle").css("display", "block");}
	else 				{$("#detalle").css("display", "none");}
}

function cambiarinventario()
{
	document.agregarform.stinventario.value=document.agregarform.stinventariob.value;
}

function cambiarinventarioedit()
{
	document.editarform.stinventario.value=document.editarform.stinventariob.value;
}
/**/

function articulosfotos(codigo, tinventario, stinventario)
{
	self.name = "main"; // names current window as "main"
	var windowprops="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=yes,resizable=0,width=700,height=600";
	var ventana="articulosfotos.php?&codigo="+codigo+"&tinventario="+tinventario+"&stinventario="+stinventario;
	window.open(ventana,"",windowprops); // opens remote control
}
function procesarfoto(ammcod)
{
	//onchange='javascript:copia(this.value)' 
	document.getElementById("foto"+ammcod).innerHTML="<input name='userfile' id='userfile' type='file' /><br /><br />"+
													 "<input type='submit' name='procesar' id='procesar' value='Procesar' />"+
													 "<label>"+
													 "<input type='button' name='cancel' id='button' value='Cancelar' onclick='javascript:cancelarprocesarfoto(\""+ammcod+"\")' />"+
													 "</label>"+
													 "<input type='hidden' name='ammcod' id='ammcod' value='" + ammcod + "'>";
	document.getElementById("foto"+ammcod).style.visibility="visible";
	
}

function cancelarprocesarfoto(ammcod)
{
	document.getElementById("foto"+ammcod).style.visibility="hidden";
	document.fotos.ammcod.value="";
}

function borrarfoto(letra,codigo,codmultimedia,tinventario,stinventario) 
{
	if (confirm('Seguro que desea borrar la foto: ' + codigo + '?'))
	{
		location.href="borrarfoto.php?&letra="+letra+"&codigo="+codigo+"&codmultimedia="+codmultimedia+"&tinventario="+tinventario+"&stinventario="+stinventario;
	}
}

function cargaralmacen(aarumb, tipo, aarcod)
{
	var param = [];
	param['tipo']=tipo;
	param['aumcodtem']=aarumb;
	param['aarcod']=aarcod;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargaralmacen.php',
		beforeSend:function(){
			$('#almacenes').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#almacenes').html(html);
		}
	});	
}

function activar(articulo, status)
{
	var text = '';
	if(status == '01')//activo =>desactivo
	{ text = 'Desea Desactivar el articulo';}
	else if(status == '02')//activo =>desactivo
	{ text = 'Desea Activar el articulo';}
	if (confirm(text))
	{
		var param = [];
		param['aarcod']=articulo;
		param['aarsts']=status;
		ejecutasqld("articulostatus.php",param);
		document.body.style.cursor="wait";
		location.reload();
	}	
}