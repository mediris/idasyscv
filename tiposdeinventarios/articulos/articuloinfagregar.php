<?php 
/*
 *jDavila
 *16/02/2012
 *modificado:22/02/2012
 *@tipo inventario modificado de select a check's
 */
session_start();
include("../../conectar.php");
	/*instancia de clase*/
	$iu = new inf_tinventario($Compania, $tinventario); 
	//$nivt=$iu->batinivn;
	$nivt=$iu->batinivt;
	$desc=$iu->batides;
	$esc=$iu->batiecat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>agregar articulo</title>
<link href="../../style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../../javascript/jquery.js" type="text/javascript"></script>
<script language="JavaScript" src="../../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../../calendario/javascripts.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:agregar();cargaralmacen(document.getElementById('aarumb').value, '1', '');">

	<div id="agregardiv" class="white_content_tnye">
        <form id="agregarform" name="agregarform" method="post" action="" class="form">
            	<table width="100%"  border="0">
                    <tr>
                        <td  align="left" scope="col"><label>Art�culo:</label></td>
                        <td  align="left" scope="col"><div>
                            <!-- iv05fp -->
                            <input name="aarcod" type="text" id="aarcod" size="15" maxlength="15" onChange="javascript:buscanivelart(this.value, <?php echo $esc;?> )">  
                            </div>
                        </td>
                        <td colspan="2" align="left" id="erraarcod" scope="col">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" scope="col"><label>Sub-Tipo de Inventario:</label></td>
                        <td  align="left" scope="col"><div>
                            <!-- iv05fp -->
                            <select name="stinventariob" id="stinventariob"  onChange="cambiarinventario()">
                              <?php ECHO $sql="Select asicod, asides from IV03FP where aticod='$tinventario' order by asicod ASC";
                                    $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                                    while(odbc_fetch_row($result1)){			
                                        $asicod=trim(odbc_result($result1,1));
                                        $asides=trim(odbc_result($result1,2));
                                        if (strlen($asicod)==$nivt) {
                             ?>                              
                                          <option value= "<?php echo $asicod; ?>" ><?php echo $asides; ?></option>
                              <?php }} ?>
                            </select>
                            </div>
                        </td>
                      </tr>
                      <tr>
                        <td align="left" scope="col"><label>Descripci�n</label></td>
                        <td align="left" scope="col">
                            <div>
                            <!-- iv05fp -->
                            <textarea name="aardes" cols="50" rows="2" id="aardes" ></textarea></div>
                        </td>
                        <td colspan="2" align="left" id="erraardes" scope="col">&nbsp;</td>
                      </tr>

                      
                      
                </table>
                  
                <table width="100%"   id="detalle">
                    <tr>
                        <td align="left" scope="col"><h3>Detalle :</h3></td>
                        <!-- iv06fp --><td align="left" scope="col"></td>
                        <td colspan="2" align="left" id="erramdcod" scope="col">&nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td align="left" scope="col"><label>Marca</label></td>
                        <!-- iv06fp --><td align="left" scope="col"><div class="header">
                            <select name="amccod" id="amccod" >
                                <?php $sql="SELECT AMCCOD, AMCDES, AMCNOI, AMCSTS FROM iv04fp WHERE ACICOD ='$Compania' ORDER BY AMCCOD";
                                    $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                                    while(odbc_fetch_row($result1)){			
                                        $amccod=trim(odbc_result($result1,1));
                                        $amcdes=odbc_result($result1,2);?>
                                        <option value= "<?php echo $amccod; ?>" ><?php echo $amcdes ?></option>
                                <?php } ?>
                              </select>
                        </div>
                        </td>
                        <td colspan="2" align="left" id="erramdcod" scope="col">&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="left" scope="col"><label>Unidad de Medida Base</label></td>
                        <!-- iv06fp --><td align="left" scope="col"><div class="header">
                              <select name="aarumb" id="aarumb" onchange="cargaralmacen(this.value, '1', '')">
                                <?php $sql="SELECT aumcod, aumdes, aumsts FROM iv13fp WHERE ACICOD ='$Compania' ORDER BY aumcod";
										$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
										while(odbc_fetch_row($result1)){			
											$aumcod=trim(odbc_result($result1,1));
											$aumdes=odbc_result($result1,2);?>
                                			<option value= "<?php echo $aumcod; ?>" ><?php echo $aumdes ?></option>
                                <?php } ?>
                              </select>
                        </div></td>
                        <td colspan="2" align="left" id="erraarumb" scope="col">&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="left" scope="col"><label>Art�culo con uso de lote</label></td>
                        <td align="left" scope="col"><div>
                          <p>
                            <span class="header">
                            
                              <!-- iv06fp -->
                              <input name="aarlot" type="radio" value="S" checked>
                              Si
                            
                              <!-- iv06fp -->
                              <input type="radio" name="aarlot" value="N">
                              No
                            </span>          
                        </div></td>
                        <td colspan="2" align="left" id="erraarlot" scope="col">&nbsp;</td>
                      </tr>
                      <tr>
                            <td align="left" scope="col"><label>Art�culo con uso de Serial</label></td>
                            <td align="left" scope="col"><div>
                              <p>
                                <span class="header">
                           
                                <!-- iv06fp -->
                                <input name="aarser" type="radio" value="S" checked>
                                  Si
                               
                                <!-- iv06fp -->
                                <input type="radio" name="aarser" value="N">
                                  No
                                </span>          
                            </div></td>
                            <td colspan="2" align="left" id="erraariva" scope="col">&nbsp;</td>
                        </tr>
                        <tr>
                        	<td colspan="2">
                        		<div id="almacenes"></div>
                        	</td>
                        </tr>
                        <?php //list_carga_almacenes_uni_base($cid, $Compania, 1);?>
                        <tr>
                            <td colspan="4" align="left" scope="col">
                                <table width="100%"  border="0" >
                                  <tr>
                                    <td width="100%" colspan="3" scope="col"><h3>Par�metros Adicionales: </h3>
                                      <span class="header">
                                      <input type="hidden" name="tinventario" id="tinventario" value="<?php echo $tinventario;?>">          
                                      <input type="hidden" name="stinventario" id="stinventario" value="<?php echo $asicod;?>">
                                      <input type="hidden" name="stinventariold" id="stinventariold">
                                      </span></td>
                                    </tr>
                                  
                                  	<?php 
											//$sql="SELECT T1.APDCOD, T2.APDDES, T2.APDTIP, T2.APDLON, T2.APDVAL,T2.APDSEC FROM iv02fp t1, SAFIROW.MB03FP t2 WHERE t1.ACICOD ='$Compania' and t1.aticod='$tinventario' and t1.amdcod='$modulo' and T1.AMDCOD= T2.AMDCOD and T1.APDCOD= T2.APDCOD ORDER BY t1.apdcod";
                                            //echo fparametroarticulo('agregarform','detalle',$sql);
											$sql ="SELECT '','', T3.APDCOD, T3.APDDES, T3.APDTIP, T3.APDLON, T3.APDLND, T3.APDVAL, '', T3.ASBCOD, T4.ASBDES, T3.APDSEC 
												   FROM IV02FP T5, SAFIROW.MB03FP T3, SAFIROW.MB10FP T4
												   WHERE T5.ACICOD='$Compania' AND T5.ATICOD='$tinventario' AND T3.AMDCOD='$modulo' AND 
												   		 T5.APDCOD=T3.APDCOD AND T3.AMDCOD=T4.AMDCOD AND T3.ATACOD=T4.ATACOD AND 
														 T3.ASBCOD=T4.ASBCOD ORDER BY T3.ASBCOD, T3.APDSEC, T5.APDCOD";
                                        	echo fparam_adi_add_articulo('agregarform','detalle',$sql);
									?>
                                  </table>
                             </td>
                        </tr>
                </table>
                <table width="100%">
                  <tr>
    	          	<th scope="col">&nbsp;</th>
                	<th scope="col"></th>
                	<th width="23%"  scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"  value="Salir" id="cancelar"></th>
		        	<th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="agregarvalidar()" /></th>
                  </tr>
                </table>
        </form>
    </div>

<div align="center" id="agregaraftsav">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>