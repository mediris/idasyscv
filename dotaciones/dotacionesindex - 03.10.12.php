<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({
overlayOpacity: "0.5"
});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.1/media/css/demo_page.css";
			@import "../DataTables-1.9.1/media/css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.1/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
				  document.getElementById('info').style.display ="";
			      $('#info').dataTable( 
				  {
			        //inicio seccion de agrupacion
					"fnDrawCallback": function ( oSettings ) {
						if ( oSettings.aiDisplay.length == 0 )
						{
							return;
						}
				 
						var nTrs = $('#info tbody tr');
						var iColspan = nTrs[0].getElementsByTagName('td').length;
						var sLastGroup = "";
						var sLastGroup2 = "";
						/*grupo de status*/
						for ( var i=0 ; i<nTrs.length ; i++ )
						{
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[0];
							if ( sGroup != sLastGroup )
							{
								var nGroup = document.createElement( 'tr' );
								var nCell = document.createElement( 'td' );
								nCell.colSpan = iColspan;
								nCell.className = "Status";//el nombre de la comlumna (debe estar de primera..)
								nCell.innerHTML = sGroup;
								nGroup.appendChild( nCell );
								nTrs[i].parentNode.insertBefore( nGroup, nTrs[i] );
								sLastGroup = sGroup;
							}
						}
						/*subgrupo de usuario*/
						for ( var d=0 ; d<nTrs.length ; d++ )
						{
							var iDisplayIndex2 = oSettings._iDisplayStart + d;
							var sGroup2 = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex2] ]._aData[1];
							if ( sGroup2 != sLastGroup2 )
							{
								var nGroup2 = document.createElement( 'tr' );
								var nCell2 = document.createElement( 'td' );
								nCell2.colSpan = iColspan;
								nCell2.className = "Usuario";//el nombre de la comlumna (debe estar de primera..)
								nCell2.innerHTML = sGroup2;
								nGroup2.appendChild( nCell2 );
								nTrs[d].parentNode.insertBefore( nGroup2, nTrs[d] );
								sLastGroup2 = sGroup2;
							}
						}
					},
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] },{ "bVisible": false, "aTargets": [ 1 ] }
					],
					"aaSortingFixed": [[ 1, 'desc' ]],
					"aaSorting": [[ 0, 'desc' ]],
					"sDom": 'lfr<"giveHeight"t>ip',
					//fin seccion de agrupacion
					
					"bStateSave": true,
					"oLanguage": 
					{
			          "sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
					  "oPaginate": 
					  {
			            "sNext": "Siguiente",
						"sPrevious": "Anterior"
			          },
					  
			        }
			      } );
			  } );
</script>
</head>
<body>
<div id="wrapper">
  <?php include("../superior.php"); ?>
  <div id="page">
      <?php include("../validar.php");?>

<!--<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Archives</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
      </ul>
    </div>-->
	  
         <?php 
		 	$wsolicitud=0;
			
			/*nuevo*/	
			$sqlsup = "SELECT AUSCIA, AUSCOD, AUSSUP, AUSUSR FROM IDASYSW.mb20fp WHERE AUSCOD ='".$Usuario."' AND AUSCIA='".$Compania."'";
			$resultsup =@odbc_exec($cid,$sqlsup)or die(exit("Error en odbc_exec 11111"));
			$supervisor = odbc_result($resultsup, 'AUSSUP');
			if($supervisor=='S')
			{
				$CodAlm="";
				$CodAlm=trim($_GET['aalcod']);
				if(empty($CodAlm))$CodAlm='0001';
				/*buscar almacen */
				$sqltop = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp t1, iv07fp t2 WHERE       
						T1.ACICOD= T2.ACICOD and T1.AALCOD= T2.AALCOD and t1.ACICOD ='".$Compania."' 
						and T1.AUSCOD = '".$Usuario."' and t1.aalcod= '".trim($CodAlm)."'";
				$resulttop=@odbc_exec($cid,$sqltop)or die(exit("Error en odbc_exec 11111"));
				$listCodAlm = Array();
				$listDesAlm = Array();
				while(odbc_fetch_row($resulttop))
				{
					$listCodAlm[] = odbc_result($resulttop,"AALCOD");
					if(empty($CodAlm)){$CodAlm = odbc_result($resulttop,"AALCOD");}
					$listDesAlm[] =odbc_result($resulttop,"AALDES");
				}
			}
			else
			{
				/*buscar almacen */
				$sqltop = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp t1, iv07fp t2 WHERE       
						T1.ACICOD= T2.ACICOD and T1.AALCOD= T2.AALCOD and t1.ACICOD ='".$Compania."' 
						and T1.AUSCOD = '".$Usuario."' ";
				$resulttop=@odbc_exec($cid,$sqltop)or die(exit("Error en odbc_exec 11111"));
				$listCodAlm = Array();
				$listDesAlm = Array();
				$CodAlm="";
				$DesAlm="";
				while(odbc_fetch_row($resulttop))
				{
					$listCodAlm[] = odbc_result($resulttop,"AALCOD");
					$CodAlm .= odbc_result($resulttop,"AALCOD").",";
					$listDesAlm[] =odbc_result($resulttop,"AALDES");
					$DesAlm .=odbc_result($resulttop,"AALDES").", ";
				}
				if(!empty($CodAlm)){
					$CodAlm = substr($CodAlm,0, (strripos($CodAlm,",")));
				}
				if(!empty($DesAlm)){
					$DesAlm = substr($DesAlm,0,(strripos($DesAlm,',')));
				}
			}
			/*nuevo*/
				if ($solicitudpagina==0){
							if($CodAlm == ''){
								//echo "<br><br><h1><center>Ud no est� asociado a alguna unidad solicitante</center></h1><br><center><img src='../images/alert.png' alt='Alerta' width='40' height='40' border='0'></center>";
								echo "<br><br><h1><center>Ud no est� asociado a alg�n Almacen</center></h1><br><center><img src='../images/alert.png' alt='Alerta' width='40' height='40' border='0'></center>";
								echo "<br><br><center>Por favor comuniquese con el administrador del sistema</center>";
								echo "<br><h3><center><a href=http://".$Direccionip."/syscv/index.php>REGRESAR</a></center></h3>";
								exit;}
							else{
								
								$sql="SELECT t1.ACICOD, t1.AALCOD, t1.ADPCOD, t1.ATRCOD, t1.ATRNUM, t1.ATRDES, t1.ATROBS, t1.ATRFEC, t1.ATRMOR, t1.AUSCOD, t1.ADSSTS, T2.ATSDES, T2.ATSAPR
									  FROM iv35fp t1 , is15fp t2
									  WHERE  t1.acicod=t2.acicod and t1.ACICOD='$Compania' and t1.AALCOD in ($CodAlm) and  T1.ATRCOD=T2.ATSCOd ";
								//if($supervisor != 'S')$sql.= " and t1.AUSCOD = '$Usuario' ";
								$sql.= " ORDER BY t1.ADSSTS desc , t1.atrfec desc, t1.atrnum desc ";
							      
							}
							//echo $sql;
							
							$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
							$z=0;
							$lin=1;
							$limitep=$_SESSION['solicitudlineasporpaginat'];
							$pag=1;
							$primero='S';
							
  							while(odbc_fetch_row($resultt)){
											
											$jml = odbc_num_fields($resultt);
											$row[$z]["pagina"] =  $pag;
   			 								for($i=1;$i<=$jml;$i++)
        									{$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);}
											$z++;
								if ($lin>=$limitep) {$limitep+=$_SESSION['solicitudlineasporpaginat'];
													$pag++;}
								$lin++;}
								$totsol=($lin-1);
								$_SESSION['totalsolicitudes']=$totsol;
								$_SESSION['solicitudarreglo']=$row;
							    $solicitudpagina=1;
								$_SESSION['solicitudpaginas']=$pag;
								}
?>
		<div id="content3" >
        	<table width="100%"   border="0">
              <tr>
                <td width="84%" ><h1 align="center" class="title">DOTACIONES</h1>
                	<div align="center">
                    <!--<h3><?php echo $descrp;	?></h3>
                 <h5><?php 	echo "Almac�n: ".$descrpa	 ?></h5>-->
                 <form><strong>Almacen:</strong>&nbsp;
                
                    <?PHP 
                        if($supervisor =='S') 
                        {?>
                            
                            <select name="aalcod" id="aalcod" onchange="submit();">
                            <?php 
							$CodAlm2 = explode(",", trim($CodAlm));
							$CodAlm3 ='';
							foreach($CodAlm2 as $key => $value)
							{
								$CodAlm3 .= "'".$value."',";
							}
							$CodAlm3 = substr($CodAlm3,0,(strripos($CodAlm3,",")));
                            $sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania' order by AALDES ";
                            $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                            $select = '';
                            while(odbc_fetch_row($result1)){

                                $cod=trim(odbc_result($result1,1));
                                $des=trim(odbc_result($result1,2));
                                if(!empty($CodAlm))
                                {
                                    if($cod == $CodAlm){
                                        $select = ' selected="selected" ';
                                    }
                                    else{
                                        $select = '';
                                    }
                                }											
                            ?>
                                <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des." ( ".$cod." )"; ?></option>
                            <?php } ?>
                        </select>
                        <?php 	
                        }
                        else
                        {
                            echo $DesAlm; 
                        }
                    ?>
                    </form>
                 </div>
                 <hr />
                 </td>
                <td width="16%" ><div align="left">
                  <table width="100%"  border="0">
                    <tr>
						<?php if(validarAlmacenes(trim($CodAlm), $listCodAlm)){ ?>
                          <th width="36%" scope="col"><a rel="shadowbox;width=800;height=500"  href="../dotaciones/dotacionesfagregar.php?almacen=<?php echo $CodAlm;?>"><img src="../images/agregar.gif" title="Nueva Dotaci�n" width="29" height="30" border="0" /></a></th>
                        <?php }else{ ?>
                          <th width="36%" scope="col">&nbsp;</th>
                        <?php } ?>
                      <th width="30%" ><img src="../images/excel.jpg" alt="" width="25" height="25" /></th>
                      <th width="16%" ><img src="../images/pdf.jpg" alt="" width="25" height="25" /></th>
                      <th width="18%" ><img src="../images/impresora.gif" alt="" width="25" height="25" /></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>
		<div id="container">
            <div id="demo">
                <table width="100%" id="info" style="display:none">
            <thead>
              <tr>
	            <th ><div align="left"><strong>Usuario</strong></div></th>
             	<th ><div align="left"><strong>Status</strong></div></th>
                <th ><div align="left"><strong>tipo</strong></div></th>
                <th>Transacci&oacute;n</th>
                <th>Descripci&oacute;n</th>
                <th>Fecha</th>
            	<th>Usuario</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php $tipopar="";
			  		$tipotec="";
				
				$paginat=$_SESSION['solicitudarreglo'];
				//print_r($paginat);
				$pagact=$solicitudpagina;
				for($g=0; $g < (count($paginat)); $g++){
				  	
 			?>

              <tr>
               	<td><div align="left"><strong>Almacen: <?php echo alamcen(trim($paginat[$g]["AALCOD"]),$Compania);?></strong></div></td>
              	<td><div align="left"><strong>Status: <?php echo status('ADSSTS',$paginat[$g]["ADSSTS"]);?></strong></div></td>
                
                <td><div align="left"><strong>tipo: <?php echo $paginat[$g]["ATSDES"];?></strong></div></td>
                <td><div align="center"><strong><?php echo $paginat[$g]["ATRNUM"];?></strong></div></td>
                <td ><div align="center"><?php echo $paginat[$g]["ATRDES"];?></div></td>
                <td ><div align="center"><?php echo $paginat[$g]["ATRFEC"];?></div></td>
                <td ><div align="center"><?php echo $paginat[$g]["AUSCOD"];?></div></td>
                <td valign="top" ><ul id="opciones">
                    <?php if($paginat[$g]["ADSSTS"]=='01' && trim($paginat[$g]["AUSCOD"] )==trim($Usuario)){?>
                    	<li>
                        	<a rel="shadowbox;width=800;height=500" title="Editar Pedido" href="../dotaciones/dotacionesfeditar.php?&pedid=<?php echo trim($paginat[$g]["ATRNUM"]);?>&almacen=<?php echo trim($paginat[$g]["AALCOD"]);;?>&tipo=<?php echo trim($paginat[$g]["ATRCOD"]);?>">
                        		<img src="../images/editar.gif" title="Editar informaci&oacute;n" width="25" height="25" border="0">
                            </a>
                       	</li>
						<?PHP $aprovacion = buscar_requiere_aprovacion_dotacion(trim($paginat[$g]["ATSAPR"]), $modulo, $Compania, $cid);
							if(($aprovacion==1)){
						?>
                        <li>
                        	<a href="javascript:aprovacion('<?php echo trim($paginat[$g]["AALCOD"]); ?>','<?php echo trim(alamcen(trim($paginat[$g]["AALCOD"]), $Compania)); ?>','<?php echo trim($paginat[$g]["ATRNUM"]);?>','<?php echo trim($paginat[$g]["ATRCOD"]);?>')">
                            	<img src="../images/enviaremail.png" title="Enviar a aprobar" width="22" height="27" border="0">
                            </a>
                       	</li>
                        <?php }else if(($aprovacion==0)){ ?>
                        <li>
                        	<a href="javascript:confirmadotacion('<?php echo trim($paginat[$g]["AALCOD"]); ?>','<?php echo trim(alamcen(trim($paginat[$g]["AALCOD"]),$Compania)); ?>','<?php echo trim($paginat[$g]["ATRNUM"]);?>','<?php echo trim($paginat[$g]["ATRCOD"]);?>');">
                            	<img src="../images/check.gif" title="Confirmaci�n de Dotaci�n" width="22" height="27" border="0">
                            </a>
                       	</li>
                        <?php } ?>
                        <li><a href="javascript:eliminar('<?php echo trim($paginat[$g]["ATRNUM"]);?>','<?php echo trim($paginat[$g]["AALCOD"]);?>','<?php echo trim($paginat[$g]["ATRCOD"]);?>')"><img src="../images/eliminar.gif" title="Eliminar" width="25" height="25" border="0"></a></li>
					<?php } ?>
                    <li><a href="javascript:requerimverdetalle('<?php echo trim($paginat[$g]["ATRNUM"]);?>','<?php echo trim($paginat[$g]["AALCOD"]);?>','<?php echo trim($paginat[$g]["ATRCOD"]);?>')"><img src="../images/ver.png" title="Ver Detalle" width="31" height="28" border="0"></a></li>
                    <?php if($paginat[$g]["ADSSTS"]=='03' ){?>
                    	<li><a href="javascript:requerimimprimir('<?php echo trim($paginat[$g]["ATRNUM"]); ?>', '<?php echo trim($paginat[$g]["AALCOD"]); ?>','<?php echo trim($paginat[$g]["ATRCOD"]);?>')"><img src="../images/impresora.gif" alt="Imprimir informaci&oacute;n" width="30" height="25" border="0" /></a></li>
		            <?php }?>
                  </ul></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
			
          </td>
          </tr>
          </table>
	    </div>
	  </div>
  </div>
		<!-- end #content -->
		<!-- end #sidebar -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
