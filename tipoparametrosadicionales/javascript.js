/*
 * jDavila
 * 28/02/12
 */
 
function agregar()
{
	document.agregarform.atacod.value="";
	document.agregarform.atades.value="";
	$("#agregaraftsav").hide(1);
}

/*
jDavila
28/02/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "tipoparametroagregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
 * jDavila
 * 28/02/2012
 */
function editar(atacod) {

	var param = [];
	param['atacod']=atacod;

	ejecutasqlp("tipoparametroinformacionphp.php",param);

	for(i in gdata){
		document.editarform.hatacod.value=gdata[i].ATACOD;
		document.getElementById("wsatacod").innerHTML=gdata[i].ATACOD;
		document.editarform.atades.value=gdata[i].ATADES;
	};
	$("#editaraftsav").hide(1);
}

/*
 *jDavila
 *28/02/2012
 */
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "tipoparametroeditarvalidarphp.php",
			data: $("#editarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#editarform").hide(1000);
		$("#editaraftsav").show(1000);	
	}
}


/*
 *jDavila
 *28/02/2012
 */
function eliminar(tipopar) {
	if (confirm('Seguro que desea borrar el tipo de parametro ' + tipopar + '?'))
	{
		var param = [];
		param['atacod']=tipopar;
		ejecutasqld("tipoparametroeliminar.php",param);
		location.reload();
	}
}
