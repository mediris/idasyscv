/*
 * jDavila
 * 28/02/12
 */
function agregar()
{
	document.agregarform.asbcod.value="";
	document.agregarform.asbdes.value="";
	$("#agregaraftsav").hide(1);
}

/*
jDavila
28/02/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "subtipoparametroagregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
 * jDavila
 * 01/03/2012
 */
function editar(atacod, asbcod) {

	var param = [];
	param['atacod']=atacod;
	param['asbcod']=asbcod;

	ejecutasqlp("subtipoparametroinformacionphp.php",param);

	for(i in gdata){
		document.editarform.hasbcod.value=gdata[i].ASBCOD;
		document.getElementById("wsasbcod").innerHTML=gdata[i].ASBCOD;
		document.editarform.asbdes.value=gdata[i].ASBDES;
		document.editarform.atacod.value=gdata[i].ATACOD;
	};
	$("#editaraftsav").hide(1);
}

/*
 *jDavila
 *01/03/2012
 */
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "subtipoparametroeditarvalidarphp.php",
			data: $("#editarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#editarform").hide(1000);
		$("#editaraftsav").show(1000);	
	}
}


/*
 *jDavila
 *01/03/2012
 */
function eliminar(tipopar, subtipo) {
	if (confirm('Seguro que desea borrar el sub tipo de parametro ' + subtipo + ' perteneciente al tipo parametro  ' + tipopar + ' ?'))
	{
		var param = [];
		param['atacod']=tipopar;
		param['asbcod']=subtipo;
		ejecutasqld("subtipoparametroeliminar.php",param);
		location.reload();
	}
}
