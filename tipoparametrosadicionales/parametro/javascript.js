/*
 * jDavila
 * 01/03/12
 */
function agregar()
{
	document.agregarform.apdcod.value="";
	document.agregarform.apddes.value="";
	document.agregarform.asbcod.value="";
//	document.agregarform.atacod.value="";
//	document.agregarform.atades.value="";
	document.agregarform.apdsec.value="";
	document.agregarform.apdtip.value="";
	document.agregarform.apdlon.value="";
	document.agregarform.apdlnd.value="";
	document.agregarform.apdval.value="";
	
	$("#agregaraftsav").hide(1);
}

/*
 * jDavila
 * 01/03/2012
 */
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "parametroagregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
 * jDavila
 * 01/03/2012
 */
function editar(atacod, apdcod) {

	var param = [];
	param['atacod']=atacod;
	param['apdcod']=apdcod;

	ejecutasqlp("parametroinformacionphp.php",param);

	for(i in gdata){
		//t1.AMDCOD, t1.APDCOD, t1.APDSEC, t1.ATACOD, t1.APDDES, t1.APDTIP, t1.APDVAL, t1.APDSTS, t1.APDLON, t1.APDLND,t2.ASBCOD
		document.editarform.hapdcod.value=gdata[i].APDCOD;
		document.getElementById("wsapdcod").innerHTML=gdata[i].APDCOD;
		document.editarform.apddes.value=gdata[i].APDDES;
		document.editarform.asbcod.value=gdata[i].ASBCOD;
		document.editarform.atacod.value=gdata[i].ATACOD;
		document.editarform.atades.value=gdata[i].ATADES;
		document.editarform.apdsec.value=gdata[i].APDSEC;
		document.editarform.apdtip.value=gdata[i].APDTIP;
		document.editarform.apdlon.value=gdata[i].APDLON;
		document.editarform.apdlnd.value=gdata[i].APDLND;
		document.editarform.apdval.value=gdata[i].APDVAL;
	};
	$("#editaraftsav").hide(1);
}

/*
 *jDavila
 *02/03/2012
 */
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "parametroeditarvalidarphp.php",
			data: $("#editarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#editarform").hide(1000);
		$("#editaraftsav").show(1000);	
	}
}


/*
 *jDavila
 *01/03/2012
 */
function eliminar(tipo, descp, parametro) {
	if (confirm('Seguro que desea borrar el parametro ' + parametro + ' que pertenece a ' + tipo + ' - ' + descp + ' ?'))
	{
		var param = [];
		param['tipo']=tipo;
		param['descrp']=descp;
		param['parametro']=parametro;
		ejecutasqld("parametroeliminar.php",param);
		location.reload();
	}
}

/*
 * jDavila
 * 02/03/2012
 * @metodo: realiza los cambios en segun el tipo de dato, en el area de agregar (tipoparametrosadicionales/parametro/parametrofagregar.php)
 */

function valoresedit() 
{
	if(document.editarform.apdtip.value=="1" || document.editarform.apdtip.value=="6")//alfanumerico,lista
	   {
		   	document.editarform.apdlon.value="";
			document.editarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="visible";
			document.getElementById("visible2").style.visibility="hidden";
	   }
		
	else if(document.editarform.apdtip.value=="2")//numerico
	   {
		   	document.editarform.apdlon.value="";
			document.editarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="visible";
			document.getElementById("visible2").style.visibility="visible";
	   }
	else if(document.editarform.apdtip.value=="3")//fecha
	   {
		   	document.editarform.apdlon.value=10;
			document.editarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="hidden";
			document.getElementById("visible2").style.visibility="hidden";
	   }
	else if(document.editarform.apdtip.value=="4")//hora
	   {
		   	document.editarform.apdlon.value=8;
			document.editarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="hidden";
			document.getElementById("visible2").style.visibility="hidden";
	   }
	else if(document.editarform.apdtip.value=="5")//pregunta
	   {
		   	document.editarform.apdlon.value=1;
			document.editarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="hidden";
			document.getElementById("visible2").style.visibility="hidden";
	   }
	else
		{
			document.editarform.apdlon.value="";
			document.editarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="hidden";
			document.getElementById("visible2").style.visibility="hidden";
		}
}

/*
 * jDavila
 * 02/03/2012
 * @metodo: realiza los cambios en segun el tipo de dato, en el area de agregar (tipoparametrosadicionales/parametro/parametrofeditar.php)
 */
function valoresagregar() 
{
	if(document.agregarform.apdtip.value=="1" || document.agregarform.apdtip.value=="6")//alfanumerico,lista
	   {
		   	document.agregarform.apdlon.value="";
			document.agregarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="visible";
			document.getElementById("visible2").style.visibility="hidden";
	   }
		
	else if(document.agregarform.apdtip.value=="2")//numerico
	   {
		   	document.agregarform.apdlon.value="";
			document.agregarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="visible";
			document.getElementById("visible2").style.visibility="visible";
	   }
	else if(document.agregarform.apdtip.value=="3")//fecha
	   {
		   	document.agregarform.apdlon.value=10;
			document.agregarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="hidden";
			document.getElementById("visible2").style.visibility="hidden";
	   }
	else if(document.agregarform.apdtip.value=="4")//hora
	   {
		   	document.agregarform.apdlon.value=8;
			document.agregarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="hidden";
			document.getElementById("visible2").style.visibility="hidden";
	   }
	else if(document.agregarform.apdtip.value=="5")//pregunta
	   {
		   	document.agregarform.apdlon.value=1;
			document.agregarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="hidden";
			document.getElementById("visible2").style.visibility="hidden";
	   }
	else
		{
			document.agregarform.apdlon.value="";
			document.agregarform.apdlnd.value="";
			document.getElementById("visible1").style.visibility="hidden";
			document.getElementById("visible2").style.visibility="hidden";
		}
}