<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>editar transaccion</title>
<link href="../style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:editar('<?php echo trim($atlcod); ?>','<?php echo trim($asonro); ?>', '<?php echo trim($auncod);?>');cargarparametros(document.getElementById('atlcod'),'<?php echo trim($asonro); ?>');">
<div id="agregardiv">
	<form id="agregarform" name="agregarform" method="post" class="form" action="">
    <input name="auncod" id="auncod" type="hidden" value="" />
	   <table class="tabla1">
            <tr>
                <td width="29%"  scope="col"><label>Numero Solicitud</label></td>
                <td width="32%" scope="col">
                    <input name="asonro" type="hidden" id="asonro" value="" /> 
                    <div align="left" id="divasonro"></div>
                </td>
                <td colspan="2" id="errasonro"  scope="col">&nbsp;</td>
            </tr>
            <tr>
                <td width="29%"  scope="col"><label>Tipo Solicitud</label></td>
                <td width="32%" scope="col">
                	<input type="hidden" name="atlcod" id="atlcod" value="" />
                    <div align="left" id="divatlcod"></div>
                </td>
                <td colspan="2" id="erratlcod"  scope="col">&nbsp;</td>
            </tr>
            <tr>
                <td scope="col"><label>Descripci&oacute;n</label></td>
                <td scope="col"><div align="left">
                	<textarea name="asoobs"  id="asoobs" cols="50" rows="2" maxlength="1000"/></textarea>
                    </div>
                </td>
                <td colspan="2" id="errasoobs" scope="col">&nbsp;</td>
            </tr>
            <tr>
            	<td colspan="4">
                	<table width="100%"  border="0" >
                      <tr>
                        <td width="100%" colspan="3" scope="col"><h3>Parámetros Adicionales: </h3>
                          <span class="header">
                          </span></td>
                        </tr>
                      	<tr>
                        	<td>
                            <div id="parametros">
                            </div>
                            </td>
                        </tr>
                      </table>
                </td>
            </tr>
            <tr>
                <th scope="col">&nbsp;</th>
                <th scope="col"></th>
                <th width="18%" scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Salir" id="cancelar"></th>
                <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="editarvalidar()" /></th>
            </tr>
        </table>  </form></div>
     </div>
<div align="center" id="agregaraftsav" style="display:none">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>