/*
jDavila
03/04/12
*/
function agregar()
{
	document.agregarform.atlcod.value="";
	//document.agregarform.asonro.value="";
	document.agregarform.asoobs.value="";

	$("#aftsav").hide(1);
}

/*
jDavila
05/10/2012
*/
function agregarvalidar(){	

	document.getElementById('erratlcod').innerHTML = "";
	//document.getElementById('errasonro').innerHTML = "";
	document.getElementById('errasoobs').innerHTML = "";
	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "solicitudagregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo){
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';		
		}
	}
	
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
jDavila
11/10/2012
*/
function editar(tipo, num, uni) {

	
	var param = [];
	param['atlcod']=tipo;
	param['asonro']=num;
	param['auncod']=uni;
	ejecutasqlp("solicitudinformacionphp.php",param);

	for(i in gdata)
	{
		document.agregarform.asonro.value=gdata[i].ASONRO;
		document.getElementById("divasonro").innerHTML=gdata[i].ASONRO;
		document.agregarform.atlcod.value=gdata[i].ATLCOD;
		document.getElementById("divatlcod").innerHTML=gdata[i].ATLDES;
		document.agregarform.asoobs.value=gdata[i].ASOOBS;
		document.agregarform.auncod.value=gdata[i].AUNCOD;
	};
	$("#aftsav").hide(1);
}

/*
jDavila
15/10/2012
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "solicitudeditarvalidarphp.php",
			data: $("#agregarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);
					}
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo){
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';		
		}
	}
	
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}

}

/*
jDavila
30/03/12
*/
function eliminar(atl, num, uni) {
	if (confirm('Seguro que desea borrar la Solicitud ' + num +'?'))
	{
		var param = [];
		param['atlcod']=atl;
		param['asonro']=num;
		param['auncod']=uni;
		ejecutasqld("solicitudeliminar.php",param);
		location.reload();
	}
}


/**/
function finalizarreq()
{		
	$("#agregaraftsav").hide(1000);		
	parent.location.reload();
	parent.Shadowbox.close();
}	

function capturararticulo(code)
{
	
	$.get ("buscararticulo.php", { code: code },function(resultado)
	{
			if(resultado == false)
			{
				alert("No existe ningun articulo para tipo de requisición");
				document.getElementById("aarcod").value=0;
				$("#aarcod").attr("disabled",true);
			}
			else
			{
				$("#aarcod").attr("disabled",false);
				document.getElementById("aarcod").options.length=1;
				$('#aarcod').append(resultado);
			}
		}
	);
}
/**/
function cargarparametros(obj,num)
{
	//if(num=='')num=0;
	var param = [];
	var atrnum = num;
	var value = $('#'+obj.id).val();
	if (value==0){
		value = 99999999;
	}
	//alert(value+"/"+atrnum);
	param['atlcod']=value;
	param['num']=atrnum;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarparametros.php',
		beforeSend:function(){
			$('#parametros').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#parametros').html(html);
		}
	});
}


function choiceart(targ,selObj,restore,e){ //v3.0

	dat = selObj.options[selObj.selectedIndex].value;
	var datos = dat.split("@");
	/*document.getElementById("aarumb").value=datos[1];*/
	combo_uni(datos[0], datos[1]);
	//alert(document.getElementById("aarumb").value + "-" +datos[1]);
	activaimagengrabar(1);
}


function activaimagengrabar(num){ 
	//alert("*-*"+document.getElementById("aslcan").value+"**");
	document.getElementById("aslcan").focus();
	if ((document.getElementById("aslcan").value !==0) && (document.solicitudes.aarcod.selectedIndex!=='')) {
	//if ((document.getElementById("aslcan").value !='') && (document.solicitudes.aarcod.selectedIndex!=='')) {
		document.getElementById("imagengrabar").style.display='block';}
	if ((document.getElementById("aslcan").value == 0) || (document.solicitudes.aarcod.selectedIndex=='')) {
	//if ((document.getElementById("aslcan").value == '') || (document.solicitudes.aarcod.selectedIndex=='')) {
		document.getElementById("imagengrabar").style.display='none'}
}
function esenter(e) {
	var tecla;
	if (window.event) {tecla = window.event.keyCode;}
	else if (e) tecla = e.which;
	else return false;
	if (tecla == 13)   	{
		if ((document.getElementById("aslcan").value !=0) && (document.solicitudes.aarcod.selectedIndex!='')) {
		//alert("***"+document.getElementById("aslcan").value+"**");
		//if (((document.getElementById("aslcan").value !="")||(document.getElementById("aslcan").value !=" ")) && (document.solicitudes.aarcod.selectedIndex!='')) {
			grabardetallereq(); 	
			return false;
		}
	}
	else{return false;  }

}


//Requerimiento grabar en iv16fp
	function grabardetallereq(){
				var indice4 = document.solicitudes.aarcod.selectedIndex;
    			var texto4 = document.solicitudes.aarcod.options[indice4].text;
				var value4 = document.solicitudes.aarcod.options[indice4].value;
    			//var texto5 = document.solicitudes.aarumb.value;
				var indice5 = document.solicitudes.aarumb.selectedIndex;
    			var texto5 = document.solicitudes.aarumb.options[indice5].text;
				var value5 = document.solicitudes.aarumb.options[indice5].value;
				var tran =  document.solicitudes.atrcod.value;
                cadena = "<tr>";
				cadena = cadena + "<td>" + texto4 + "</td>";
                cadena = cadena + "<td> <div align='right'>" + $("#aslcan").val() + "</div></td>";
                cadena = cadena + "<td>" + texto5 + "</td>";
                cadena = cadena + "<td><a class='elimina' a href='javascript:fn_dar_eliminar(\"" + value4 + "\",this,\"" + tran + "\");'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
				cadena =  cadena + "</tr>";
				
				$.ajax({
	   			async: false,
					type: "POST",
					dataType: "JSON",
					url: "guardardetalles.php",
					data: $("#solicitudes").serialize(),
					success: function(datos){
						vdata = parseJSONx(datos);}							
				});			
                sierror='N';
				for(i in vdata)	{
					if(vdata[i].campo) 	{
						sierror='S';
					}
				}
												
				if (sierror=='S')	{
									alert("Producto ya se encuentra en la dotación");
									//return false; 
									}								
				else				{
									$("#grilla").append(cadena);
									document.getElementById("aarcod").value="";
									document.getElementById("aslcan").value="";
									document.getElementById("aarumb").value="";
									combo_uni(document.getElementById("aarcod").value, document.getElementById("aarumb").value);
									activaimagengrabar();
									document.getElementById("aarcod").focus();
									}
				
            };

		function fn_dar_eliminar(cod, hcd, tran){
                $("a.elimina").click(function(){
					hcd = document.solicitudes.atrnum.value;
                    id = $(this).parents("tr").find("td").eq(0).html();
                    respuesta = confirm("Desea eliminar el Articulo: " + id);
                    if (respuesta){									
                            $(this).parents("#grilla tr").remove();
							var param = [];
							param['atrnum']=hcd;
							param['aarcod']=cod;
							param['atrcod']=tran;
							$.ajax({
	   						async: false,
        					type: "POST",
							dataType: "JSON",
        					url: "eliminardetalle.php",
        					data: convertToJson(param),
        					success: function(datos){
								alert("Articulo " + id + " eliminado")
							}							
                        })
                    }
                });
            };
			
			
			
/*
 * @jDavila
 *13/04/2012
 */
function combo_uni(art, medi)
{
	var param = [];
	param['medi']=medi;
	param['art']=art;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarunimedida.php',
		beforeSend:function(){
			$('#div_aarnum').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#div_aarnum').html(html);
		}
	});
}
function solicitudver(atl, num, uni)
{
	self.name = "main"; // names current window as "main"
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=990,height=800";
	var ventana="solicitudver.php?&atlcod="+atl+"&num="+num+"&uni="+uni+"";
	window.open(ventana,"", windowprops); // opens remote control
}

function enviarsolicitud(alt, num, uni){
	if (confirm('Seguro que desea enviar a aprobaci\u00f3n la solicitud ' + num + '?'))
	{
		var param = [];
		param['atlcod']=alt;
		param['asonro']=num;
		param['auncod']=uni;
		//ejecutasqld("enviarrequerimiento.php",param);
		ejecutasqlp("enviarsolicitud.php",param);
		
		for(i in gdata){
			if(gdata[i].EJEC == 1)//bien
			{
				var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
				OpenWindow = window.open("correo.php?&atlcod="+alt+"&asonro="+num+"&auncod="+uni, "enviodecorreo", windowprops);

				//ejecutasqla("correo.php",param);
				$("#agregaraftsav").hide(1000);		
				parent.location.reload();
				parent.Shadowbox.close();
			}else if(gdata[i].EJEC == 0)//mal
			{
				alert("No se puede enviar aprobaci\u00f3n de la Solicitud.");
			}
		}
	}
}

//aprobar
function aprobare(atl, num, uni) {
	var param = [];
	param['atlcod']=atl;
	param['asonro']=num;
	param['auncod']=uni;
	ejecutasqld("aprobarsolicitud.php",param);
	var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
	//OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);
	OpenWindow = window.open("correoaprobado.php?&atlcod="+atl+"&asonro="+num+"&auncod="+uni, "enviodecorreo", windowprops);
}

function rechazar(atl, num, uni) {
	var param = [];
	param['atlcod']=atl;
	param['asonro']=num;
	param['auncod']=uni;
	ejecutasqld("negarsolicitud.php",param);
	var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
	//OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);
	OpenWindow = window.open("correoanegado.php?&atlcod="+atl+"&asonro="+num+"&auncod="+uni, "enviodecorreo", windowprops);
}

function solicitudimprimir(atl, num, uni)
{
	self.name = "main"; // names current window as "main"
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=990,height=800";
	var ventana="solicitudpdf.php?&atlcod="+atl+"&num="+num+"&uni="+uni+"";
	window.open(ventana,"", windowprops); // opens remote control
}