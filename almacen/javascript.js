/*
jDavila
16/03/12
*/
function agregar()
{
	document.form.aalcod.value="";
	document.form.aaldes.value="";
	document.form.aalres.value="";
	document.form.aalubi.value="";
	document.form.aalmts.value="";
	document.form.aiucod.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
16/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "almacenagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
07/03/12
*/
function editar(tipo) {

	var param = [];
	param['aalcod']=tipo;
	ejecutasqlp("almaceninformacionphp.php",param);

	for(i in gdata)
	{
		document.form.aalcod.value=gdata[i].AALCOD;
		document.getElementById("wsaalcod").innerHTML=gdata[i].AALCOD;
		document.form.aaldes.value=gdata[i].AALDES;
		document.form.aalres.value=gdata[i].AALRES;
		document.form.aalubi.value=gdata[i].AALUBI;
		document.form.aalmts.value=gdata[i].AALMTS;
		document.form.aiucod.value=gdata[i].AIUCOD;
	};
	$("#aftsav").hide(1);
}

/*
jDavila
07/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "almaceneditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
16/03/12
*/
function eliminar(tipo) {
	if (confirm('Seguro que desea borrar el almacen ' + tipo + '?'))
	{
		var param = [];
		param['aalcod']=tipo;
		ejecutasqld("almaceneliminar.php",param);
		location.reload();
	}
}

/*
jDavila
22/08/2012
*/
function eliminarsus(id, aal, aaldes) {
	if (confirm('Seguro que desea borrar el usuario ' + id + '?'))
	{
		var param = [];
		param['auscod']=id;
		param['aalcod']=aal;
		param['aaldes']=aaldes;
		
		ejecutasqld("almausuarioeliminar.php",param);
		location.reload();
	}
}

function agregarusu()
{
	document.agregarform.auscod.value="";

	$("#agregaraftsav").hide(1);
}

function agregarvalidarsus(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "almausuarioagregarvalidar.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}