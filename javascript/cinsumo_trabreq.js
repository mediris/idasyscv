// JavaScript Document

            $(document).ready(function(){
                fn_dar_eliminar();
				fn_cantidad();
                //$("#frm_usu").validate();
            });
			
			function fn_cantidad(){
				cantidad = $("#grilla tbody").find("tr").length;
				$("#span_cantidad").html(cantidad);
			};
            
            function fn_agregar(){
                cadena = "<tr>";
                cadena = cadena + "<td>" + $("#aarcod2").val() + "</td>";
				cadena = cadena + "<td>" + $("#aardes2").val() + "</td>";
                cadena = cadena + "<td>" + $("#ardcan1").val() + "</td>";
                cadena = cadena + "<td>" + $("#aarumb1").val() + "</td>";
                cadena = cadena + "<td><a class='elimina'><img src='../imagenes/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
                $("#grilla tbody").append(cadena);
				
				$.ajax({
	   			async: false,
        		type: "POST",
				dataType: "JSON",
        		url: "trabreq_agregar_art.php",
        		data: $("#frm_usu").serialize(),
        		success: function(datos){//alert(datos);
				vdata = parseJSONx(datos);}							
				});			
                
                    //$.post("trabreq_agregar_art.php", {aarcod: $("#aarcod2").val(), aardes2: $("#aardes2").val(), ardcan1: $("#ardcan1").val(), aarumb1: $("#aarumb1").val()});
                fn_dar_eliminar();
				fn_cantidad();
				document.getElementById("ardcan1").value="";
				document.getElementById("aarumb1").value="";
				document.getElementById("aardes2").value="";
				document.getElementById("aarcod2").value="";
                alert("Articulo agregado");
            };
            
            function fn_dar_eliminar(){
                $("a.elimina").click(function(){
                    id = $(this).parents("tr").find("td").eq(0).html();
					cod = document.getElementById("arqnum").value;
                    respuesta = confirm("Desea eliminar el Articulo: " + id);
                    if (respuesta){
                        $(this).parents("tr").fadeOut("normal", function(){
                            $(this).remove();
                            alert("Articulo " + id + " eliminado")
							var param = [];
							param['aarcod']=id;
							param['arqnum']=cod;
							$.ajax({
	   						async: false,
        					type: "POST",
							dataType: "JSON",
        					url: "trabreq_eliminar_art.php",
        					data: convertToJson(param),
        					success: function(datos){//alert(datos);
							vdata = parseJSONx(datos);}							
							});	
								
                        })
                    }
                });
            };