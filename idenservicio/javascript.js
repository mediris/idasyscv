/*
jDavila
02/07/12
*/
function agregar()
{
	document.form.aiscod.value="";
	document.form.aisdes.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
02/07/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "servicioagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
02/07/12
*/
function editar(tipo) {

	var param = [];
	param['aiscod']=tipo;
	ejecutasqlp("servicioinformacionphp.php",param);

	for(i in gdata)
	{
		document.form.aiscod.value=gdata[i].AISCOD;
		document.getElementById("wsaiscod").innerHTML=gdata[i].AISCOD;
		document.form.aisdes.value=gdata[i].AISDES;
	};
	$("#aftsav").hide(1);
}

/*
jDavila
02/07/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "servicioeditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
02/07/12
*/
function eliminar(tipo) {
	if (confirm('Seguro que desea borrar la Identificaci\u00f3n de Servicio ' + tipo + '?'))
	{
		var param = [];
		param['aiscod']=tipo;
		ejecutasqld("servicioeliminar.php",param);
		location.reload();
	}
}


