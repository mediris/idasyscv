<?php session_start();
	include("../conectar.php");
	$arqnro = trim($_GET["num"]);
	//header("Pragma: ");
	header("Pragma: no-cache");
	header('Cache-control: ');
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Content-type: application/vnd.ms-excel");
	//header("Content-type: application/octet-stream");
	header("Content-disposition: attachment; filename=Reporte_CV.xls");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Reporte Cv</title>
	</head>
	<style>

		h1, h2, h3, h4, h5 {
			margin: 0;
			padding: 0;
			font-weight: normal;
			color: #32639A;
		}

		h1 {
			font-size: 2em;
		}

		h2 {
			font-size: 2.4em;	
		}

		h3 {
			font-size: 1.6em;
			font-style: italic;
		}

		h4 {
			font-size: 1.6em;
			font-style: italic;
			color: #FFF;
		}

		h5 {
			font-size: 1.0em;
			font-style: italic;
			color: #666;
		}

		#background-image
		{
			font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
			font-size: 10px;
			margin: 0px;
			width: 100%;
			text-align: left;
			border-collapse: collapse;
		}
		#background-image th
		{
			padding: 12px;
			font-weight: normal;
			font-size: 12px;
			color: #339;
			border-bottom-style: solid;
			border-left-style: none;
			text-align: center;
		}
		#background-image td
		{
			color: #669;
			border-top: 1px solid #fff;
			padding-right: 4px;
			padding-left: 4px;
		}
		#background-image tfoot td
		{
			font-size: 9px;
		}
		#background-image tbody 
		{

			background-repeat: no-repeat;
			background-position: left top;
		}
		#background-image tbody td
		{
			background-image: url(images/backn.png);
		}
		* html #background-image tbody td
		{
			/* 
			   ----------------------------
				PUT THIS ON IE6 ONLY STYLE 
				AS THE RULE INVALIDATES
				YOUR STYLESHEET
			   ----------------------------
			*/
			filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
			background: none;
		}
	</style>
	<body>
		<?php 
	 		$wsolicitud=0;
			if ($solicitudpagina==0){
				
				$sql="SELECT T3.AARUMB,T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS FROM IV05FP T1, IV06FP T3, IV13FP T4 WHERE T1.ACICOD='".$Compania."' AND T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD AND T1.ACICOD=T4.ACICOD AND T3.AARUMB=T4.AUMCOD AND ( T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD='".$aalcod."' GROUP BY T2.AARCOD ORDER BY T2.AARCOD ) ) ";
				$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));

				$z=0;
				$tothon=0;
				$totest=0;
				$lin=1;
				$limitep=$_SESSION['solicitudlineasporpaginat'];
				$pag=1;
				$primero='S';
				
				$anio = date("Y");
				$mes1 = date("m");

				while(odbc_fetch_row($resultt)){
					
					$artcod = odbc_result($resultt,'AARCOD');
					$atrdes = odbc_result($resultt,'AARDES');
					$exp = substr($atrdes ,(strripos($atrdes,"(")) );
					$aumdes = odbc_result($resultt,'AUMDES');
							
					$sql2="SELECT T4.ACICOD, T4.AALCOD, T4.ADPCOD, T4.ATRCOD, (T5.ATRDES) as ATRNOM, T5.ATRSIG, T4.ATRNUM, T4.ATRDES, T4.ATRFEC, T4.AUSCOD, T4.ATROBS, T3.ATRSEC, T3.ATRART,('".$atrdes."') AS AARDES,  T3.ATRCAN, T3.ATRUMB, ('".$aumdes."') AS AUMDES, T7.AUBCOD,(SELECT T6.AAPVLA FROM SAFIROW.IV38FP T6 WHERE T6.ACICOD=T3.ACICOD AND T6.ATRCOD=T3.ATRCOD AND T6.ATRNUM=T3.ATRNUM AND T6.AAPSEC=1 ) AS NGIA , (SELECT T6.AAPVLA FROM SAFIROW.IV38FP T6 WHERE T6.ACICOD=T3.ACICOD AND T6.ATRCOD=T3.ATRCOD AND T6.ATRNUM=T3.ATRNUM AND T6.AAPSEC=2 ) AS TGIA , (SELECT T6.AAPVLA FROM SAFIROW.IV38FP T6 WHERE T6.ACICOD=T3.ACICOD AND T6.ATRCOD=T3.ATRCOD AND T6.ATRNUM=T3.ATRNUM AND T6.AAPSEC=3 ) AS FLLE , (SELECT T6.AAPVLA FROM SAFIROW.IV38FP T6 WHERE T6.ACICOD=T3.ACICOD AND T6.ATRCOD=T3.ATRCOD AND T6.ATRNUM=T3.ATRNUM AND T6.AAPSEC=4 ) AS NEXP FROM SAFIROW.IV16FP T3 INNER JOIN SAFIROW.IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM ) INNER JOIN SAFIROW.IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG='+') LEFT JOIN  SAFIROW.IV47FP T7 ON (T3.ACICOD=T7.ACICOD AND T3.AALCOD=T7.AALCOD AND T3.ATRART=T7.AARCOD) WHERE T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND T3.ATRART = '".$artcod."' AND T4.ATRCOD IN ('0002', '0008', '0009') ORDER BY T3.ATRART";
					$resultt2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111 - ".odbc_error($resultt2) ));
					
					while(odbc_fetch_row($resultt2)){
						$jml = odbc_num_fields($resultt2);
						$row[$z]["pagina"] =  $pag;
						for($i=1;$i<=$jml;$i++){	if (odbc_field_name($resultt2,$i)=='ARGTTE') {$totest+=odbc_result($resultt2,$i);}
							if (odbc_field_name($resultt2,$i)=='ARGTTM') {
								$tothon+=odbc_result($resultt2,$i);
							}
							$row[$z][odbc_field_name($resultt2,$i)] =  odbc_result($resultt2,$i);
						}
						$z++;
						if ($lin>=$limitep){
							$limitep+=$_SESSION['solicitudlineasporpaginat'];
							$pag++;
						}
						$lin++;
					}
				}
				
				$totsol=($lin-1);
				$_SESSION['totalsolicitudes']=$totsol;
				$_SESSION['solicitudarreglo']=$row;
				$solicitudpagina=1;
				$_SESSION['solicitudpaginas']=$pag;
			}

			$paginat=$_SESSION['solicitudarreglo'];
		?>      
		
		<table width="100%" border="0">
			<tr>
				<td height="89">
					<h1>
						<?php if($Compania=='14'){?>
							<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
						<?php }else{ ?>
							<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
						<?php } ?>
   					</h1>
  					<h5>RIF:  <?php echo $Companiarif; ?></h5>
  				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" id="background-image" >	
  						<thead>
  							<tr>
        						<th colspan="9" scope="col">
        							<h3>Reporte de CV</h3>
        						</th>
    						</tr>
    						<tr>
        						<th colspan="9" scope="col">
        							<h4>Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></h4>
        						</th>
    						</tr>
  							<tr>
        						<th colspan="9" scope="col">
        							<h3>Almac�n: <?php echo alamcen($aalcod, $Compania);?></h3>
        						</th>
    						</tr>
    					</thead>
    				</table>
    				<table border="1">
    					<thead>
  							<tr>
        						<th scope="col">Fecha Llegada</th>
        						<th scope="col">C�d. Expediente</th>
        						<th scope="col">Transacci�n</th>        
    							<th scope="col">Gu�a</th>
    							<th scope="col">N� Gu�a</th>
        						<th scope="col">C�digo</th>
        						<th scope="col">Descripci�n</th>
        						<th scope="col">Cant.</th>
        						<th scope="col">Ubicaci�n</th>
        						<th scope="col">Fecha Salida</th>
    							<th scope="col">Transacci�n</th>
        						<th scope="col">Cant.</th>
        						<th scope="col">Destino</th>
        						<th scope="col">Retirado Por</th>
        						<th scope="col">Observaciones</th>
    						</tr>
  						</thead>
   						<tbody>
							<?php
                				$pagact=$solicitudpagina;
                				$part= 1;
                				for($g=0; $g < (count($paginat)); $g++){
	                    			
	                    			if(strripos($paginat[$g]["AARDES"],"(") > 0){
	                        			$exp = substr($paginat[$g]["AARDES"] ,(strripos($paginat[$g]["AARDES"],"(")+1), (strripos($paginat[$g]["AARDES"],")")-strripos($paginat[$g]["AARDES"],"(")-1)  );
	                    			}else{
	                        			$exp = '-';
	                    			}

                					$sql3=" SELECT T4.ACICOD, T4.AALCOD, T4.ADPCOD, T4.ATRCOD, T5.ATRDES, T5.ATRSIG, T4.ATRNUM, T4.ATRDES as REPOR, T4.ATRFEC, T4.AUSCOD, T4.ATROBS, T3.ATRSEC, T3.ATRART,('".$paginat[$g]["AARDES"]."') AS AARDES,  T3.ATRCAN, T3.ATRUMB, ('".$paginat[$g]["AUMDES"]."') AS AUMDES, T7.AUBCOD, T8.AISDES, T8.AISCOD, T9.ADSNRO FROM SAFIROW.IV16FP T3 INNER JOIN SAFIROW.IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM ) INNER JOIN SAFIROW.IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG IN ('-','+') ) LEFT JOIN  SAFIROW.IV47FP T7 ON (T3.ACICOD=T7.ACICOD AND T3.AALCOD=T7.AALCOD AND T3.ATRART=T7.AARCOD) INNER JOIN SAFIROW.IV35FP T9 ON (T3.ACICOD=T9.ACICOD AND T3.AALCOD=T9.AALCOD AND T4.ATRCOD=T9.ATRCOD AND T4.ATRNUM=T9.ATRNUM ) INNER JOIN SAFIROW.IV42FP T8 ON (T3.ACICOD=T8.ACICOD AND T9.AISCOD=T8.AISCOD ) WHERE T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND T3.ATRART = '".$paginat[$g]["ATRART"]."' AND  T4.ATRCOD IN ('0001', '0003', '0004', '0006') ORDER BY T3.ATRART , T4.ATRFEC ";
                    				$resultt3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111 - ".odbc_error($resultt3) ));


                    				$sqlNum = "SELECT COUNT(*) AS COUNT FROM SAFIROW.IV16FP T3 INNER JOIN SAFIROW.IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM ) INNER JOIN SAFIROW.IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG IN ('-','+') ) LEFT JOIN  SAFIROW.IV47FP T7 ON (T3.ACICOD=T7.ACICOD AND T3.AALCOD=T7.AALCOD AND T3.ATRART=T7.AARCOD) INNER JOIN SAFIROW.IV35FP T9 ON (T3.ACICOD=T9.ACICOD AND T3.AALCOD=T9.AALCOD AND T4.ATRCOD=T9.ATRCOD AND T4.ATRNUM=T9.ATRNUM ) INNER JOIN SAFIROW.IV42FP T8 ON (T3.ACICOD=T8.ACICOD AND T9.AISCOD=T8.AISCOD ) WHERE T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND T3.ATRART = '".$paginat[$g]["ATRART"]."' AND  T4.ATRCOD IN ('0001', '0003', '0004', '0006')";
                    				$numero = odbc_exec($cid,$sqlNum);

	                    			$num_rows = odbc_result($numero,"COUNT");
	                     			if($num_rows>0){
	                        			$i=0;
	                        			while(odbc_fetch_row($resultt3)){
	                            			if($i==0){ ?>
	                            				
	                            				<tr>
	                            					<td scope="col" rowspan="<?php echo $num_rows;?>" valign="top" style="text-align: center;"><div><?php echo $paginat[$g]["FLLE"]!=''?formatDate($paginat[$g]["FLLE"], 'dd.mm.aaaa', 'aaaa/mm/dd'):formatDate($paginat[$g]["ATRFEC"], 'aaaa-mm-dd', 'aaaa/mm/dd');?></div></td>
	                                				<td scope="col" rowspan="<?php echo $num_rows;?>" valign="top" style="text-align: center;"><div><?php echo $paginat[$g]["NEXP"]!=''?$paginat[$g]["NEXP"]:$exp; ?></div></td>
	                            					<td scope="col" rowspan="<?php echo $num_rows;?>" valign="top" style="text-align: center;"><div><?php echo $paginat[$g]["ATRNOM"];?></div></td>
	                                				<td scope="col" rowspan="<?php echo $num_rows;?>" valign="top" style="text-align: center;"><div><?php echo $paginat[$g]["TGIA"]!=''?$paginat[$g]["TGIA"]:'-';?></div></td>
	                                				<td scope="col" rowspan="<?php echo $num_rows;?>" valign="top" style="text-align: center;"><div><?php echo $paginat[$g]["NGIA"]!=''?$paginat[$g]["NGIA"]:'-';?></div></td>
	                                				<td scope="col" rowspan="<?php echo $num_rows;?>" valign="top" style="text-align: center;"><div>&nbsp;<?php echo $paginat[$g]["ATRART"];?></div></td>
	                                				<td scope="col" rowspan="<?php echo $num_rows;?>" valign="top" style="text-align: center;"><div><strong><?php echo $paginat[$g]["AARDES"];?></strong></div></td>
	                            					<td scope="col" rowspan="<?php echo $num_rows;?>" valign="top" style="text-align: center;"><div><?php echo number_format($paginat[$g]["ATRCAN"],2,",",".");?></div></td>
	                            					<td scope="col" rowspan="<?php echo $num_rows;?>" valign="top" style="text-align: center;"><div><?php echo $paginat[$g]["AUBCOD"];?></div></td>
	                       					<?php }  ?>
	                        
	                        						<td scope="col" style="text-align: center;"><div><?php echo formatDate(odbc_result($resultt3,"ATRFEC"), 'aaaa-mm-dd', 'aaaa/mm/dd');?></div></td>
			                        				<td scope="col" style="text-align: center;"><div><?php echo odbc_result($resultt3,"ATRDES")." (".odbc_result($resultt3,"ADSNRO").")";?></div></td>
	                        						<td scope="col" style="text-align: center;"><div><?php echo number_format(odbc_result($resultt3,"ATRCAN"),2,",",".");?></div></td>
	                        						<td scope="col" style="text-align: center;"><div><?php echo odbc_result($resultt3,"AISDES");?></div></td>
	                    							<td scope="col" style="text-align: center;"><div><?php echo odbc_result($resultt3,"REPOR");?></div></td>
	                        						<td scope="col" style="text-align: center;"><div><?php echo odbc_result($resultt3,"ATROBS");?></div></td>
	                    						</tr>
	                    
	        							<?php 
	    								$i++;
	                    				}
	                 				}else{ ?>	
	                        			<tr>
	                        				<td scope="col" style="text-align: center;"><div><?php echo $paginat[$g]["FLLE"]!=''?formatDate($paginat[$g]["FLLE"], 'dd.mm.aaaa', 'aaaa/mm/dd'):formatDate($paginat[$g]["ATRFEC"], 'aaaa-mm-dd', 'aaaa/mm/dd');?></div></td>
	                            			<td scope="col" style="text-align: center;"><div><?php echo $paginat[$g]["NEXP"]!=''?$paginat[$g]["NEXP"]:$exp; ?></div></td>
	                            			<td scope="col" style="text-align: center;"><div><?php echo $paginat[$g]["ATRNOM"];?></div></td>
	                            			<td scope="col" style="text-align: center;"><div><?php echo $paginat[$g]["TGIA"]!=''?$paginat[$g]["TGIA"]:'-';?></div></td>
	                            			<td scope="col" style="text-align: center;"><div><?php echo $paginat[$g]["NGIA"]!=''?$paginat[$g]["NGIA"]:'-';?></div></td>
	                            			<td scope="col" style="text-align: center;"><div>&nbsp;<?php echo $paginat[$g]["ATRART"];?></div></td>
	                            			<td scope="col" style="text-align: center;"><div><strong><?php echo $paginat[$g]["AARDES"];?></strong></div></td>
	                            			<td scope="col" style="text-align: center;"><div><?php echo number_format($paginat[$g]["ATRCAN"],2,",",".");?></div></td>
	                            			<td scope="col" style="text-align: center;"><div><?php echo $paginat[$g]["AUBCOD"];?></div></td>                            
	                        				<td scope="col" style="text-align: center;"><div>-</div></td>
	                            			<td scope="col" style="text-align: center;"><div>-</div></td>
	                            			<td scope="col" style="text-align: center;"><div>-</div></td>
	                            			<td scope="col" style="text-align: center;"><div>-</div></td>
	                            			<td scope="col" style="text-align: center;"><div>-</div></td>
	                            			<td scope="col" style="text-align: center;"><div>-</div></td>
	                        			</tr>
	            					<?php }
	                			}
            				?>      
    					</tbody>
    				</table>
				</td>
			</tr>
		</table>
	</body>
</html>
