<?php 
session_start();
include("../conectar.php");
require_once('../tcpdf/config/lang/eng.php');
require_once('../tcpdf/tcpdf.php');

$arqnro = trim($_GET["num"]);

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('IDACA');
$pdf->SetTitle('consulta');
$pdf->SetSubject('consulta');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Despachar Pedido', PDF_HEADER_STRING);
			if ($Compania=='01') 	{$logo="../../images/logomeditronnuevo.png";}
			else					{$logo="../../images/logoidacadef20052.png";}
$pdf->SetHeaderData($logo, PDF_HEADER_LOGO_WIDTH, 'consulta', 'R.I.F.: '.$Companiarif);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// add a page
$pdf->AddPage();
// set font
$pdf->SetFont('helvetica', '', 10);

// ---------------------------------------------------------


//sum(case when t1.aslfef between '$desde' and '$hasta' then t1.ASLCTR else 0 end) as ASLCTR 
			 $sql="SELECT t1.ACICOD, t1.AALCOD, t1.AARCOD, t3.AARDES, T1.AUMCOD, T4.AUMDES, t1.ALTCOD, 
						sum(case when t1.aslfef= (SELECT min(t2.aslfef) FROM iv40fp t2 where t2.acicod=t1.acicod and t2.aalcod=t1.aalcod and t2.aarcod=t1.aarcod and t2.altcod=t1.altcod and t2.aslfef>='$desde' ) then t1.aslsaa else 0 end) as SALANT , 
						sum(case when t1.aslfef between '$desde' and '$hasta' then t1.ASLENT else 0 end) as ASLENT, 
						sum(case when t1.aslfef between '$desde' and '$hasta' then t1.ASLSAL else 0 end) as ASLSAL, 
						(SELECT SUM(ASLCTR) FROM  IV41FP T5 WHERE T1.ACICOD = T5.ACICOD AND T1.AALCOD = T5.AALCOD AND T1.AARCOD= T5.AARCOD AND T1.ALTCOD = T5.ALTCOD ) AS ASLCTR
					FROM iv40fp t1 
					INNER JOIN IV05FP T3 ON(T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD) 
					INNER JOIN IV13FP T4 ON(T1.ACICOD=T3.ACICOD AND T1.AUMCOD=T4.AUMCOD) 
				   WHERE t1.acicod='$Compania' and t1.aalcod='$aalcod' 
				   GROUP BY t1.ACICOD, t1.AALCOD, t1.AARCOD, t3.AARDES, t1.ALTCOD , T4.AUMDES,T1.AUMCOD
				   ORDER BY T3.AARDES";

			$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
			
			
						
$html.='<table width="100%" cellpadding="0" cellspacing="3">
			<tbody class="top">';

				
$html.='	<tr><td colspan="6">
				
			<table width="100%" id="background-image" >
			  <thead>
				<tr>
					<th width="30%"  scope="col">Articulo</th>
					<th width="10%" scope="col">Lote</th>
					<th width="10%" scope="col">Unidad de Medida</th>
					<th width="10%" scope="col">Saldo Anterior</th>
					<th width="10%" scope="col">Entrada</th>
					<th width="10%" scope="col">Salida</th>
					<th width="10%" scope="col">Reservada</th>
					<th width="10%" scope="col">Saldo Final</th>
				</tr>
			  </thead>
				<tbody>';
									
									while(odbc_fetch_row($result))
									{
										$acicod=trim(odbc_result($result,'ACICOD'));//
										$aalcod=trim(odbc_result($result,'AALCOD'));//
										$aarcod=trim(odbc_result($result,'AARCOD'));//
										$aardes=trim(odbc_result($result,'AARDES'));//
										$aumcod=trim(odbc_result($result,'AUMCOD'));//
										$aumdes=trim(odbc_result($result,'AUMDES'));//
										$altcod=trim(odbc_result($result,'ALTCOD'));//
										$salant=trim(odbc_result($result,'SALANT'));//
										$aslent=trim(odbc_result($result,'ASLENT'));//
										$aslsal=trim(odbc_result($result,'ASLSAL'));//
										$aslctr=trim(odbc_result($result,'ASLCTR'));//
                   					 $html.='<tr>
                                            <td scope="col"><div>'.$aardes."<strong>(".$aarcod.")</strong>".'</div></td>                
                               		 		<td scope="col"><div><strong>'.$altcod.'</strong></div></td>
                                            <td scope="col"><div align="center">'.$aumdes.'</div></td>
                                            <td scope="col"><div align="right">'.number_format($salant,2,",",".").'</div></td>
                                            <td scope="col"><div align="right">'.number_format($aslent,2,",",".").'</div></td>
                                            <td scope="col"><div align="right">'.number_format($aslsal,2,",",".").'</div></td>
                                            <td scope="col"><div align="right">'.number_format($aslctr,2,",",".").'</div></td>
                                            <td scope="col"><div align="right">'.number_format(($salant+$aslent+$aslsal),2,",",".").'</div>  </td>
                                        </tr>';
                            		}     
$html.='  </tbody>

</table></td></tr>	
			
			</tbody>
		
    </table>';


	//echo $html;
$tbl = <<<EOD
	$html
EOD;

	$pdf->writeHTML($tbl, true, false, false, false, '');
	$pdf->Output('Despacho_Pedido_'.$arqnro.'.pdf', 'I');
?>