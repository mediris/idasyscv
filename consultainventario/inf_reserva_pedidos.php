<?php 
/*
* jdavila 
* 10/03/2014
*/
session_start();
include("../conectar.php");

$aar    = $_REQUEST['aarcod'];
$aalcod = $_REQUEST['aalcod'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Detalle</title>
<link href="../style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>

</head>
<body bgcolor="#FFFFFF" background="../images/fondo idaca.jpg">
    <div align="left">
  <h2><strong><?php echo utf8_encode($Companianombre).'</strong>('.$Compania.')';  ?></h2>
  <h5><p>R.I.F.: <?php echo $Companiarif ?></p></h5></div>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" id="detalle">
          <tr>
            <td colspan="6">
                <table width="100%">
                    <tr>
                        <td align="center"><h2>Reservas en Pedidos</h2></td>
                    </tr>
                    <tr>
                        <td><h3>Almacen:<?php echo utf8_encode(alamcen($aalcod,$Compania));  ?></h3></td>
                    </tr>
                    <tr>
                        <td><h1><?php 
								$inf_art = new inf_articulo($cia, $aarcod);
								$desart = $inf_art->baardes;
								echo $desart."<strong>(".$aarcod.")</strong>"; ?></h1></td>
                    </tr>
                </table>
            </td>
          </tr>
          <tr>
          	<td><strong>Tipo de Pedido.</strong></td>
            <td><strong>Nro. Pedido.</strong></td>
            <td><strong>Fecha Solicitud</strong></td>
            <td><strong>Unidad Solicitante</strong></td>
            <td><strong>Uni. Medida</strong></td>
            <td align="right"><strong>Cantidad Despachada</strong></td>
          </tr>
          
          <?php 
		  	//buscar articulos en pedidos nuevos			
			$sql1=" SELECT T1.ACICOD, T1.ARQTIP, T3.ATQDES, T1.ARQNRO, T1.AFESOL, T2.ARDCDS, T2.ALTCOD, T2.AARUMB, T1.AUNCOD
						 FROM IS04FP T1, IS05FP T2, IS02FP T3 
						 WHERE T1.ACICOD=T2.ACICOD AND T1.ARQNRO=T2.ARQNRO AND T1.ARQTIP=T3.ATQCOD AND T3.ACICOD=T1.ACICOD AND 
					  	 T1.ACICOD='".$Compania."' AND T2.AARCOD='".$aarcod."' AND T1.ARQSTS IN('04', '05') and T1.ARDALS = '".$aalcod."' ";
			$result1=odbc_exec($cid,$sql1)or die(exit("Error en odbc_exec 1"));
			
			//cargo movimiento en iv40fp 
			while(odbc_fetch_row($result1))
			{
				$bandera1=1;
				$arqtip  = odbc_result($result1, 'ARQTIP');
				$atqdes  = odbc_result($result1, 'ATQDES');
				$arqnro  = odbc_result($result1, 'ARQNRO');
				$afesol  = odbc_result($result1, 'AFESOL');
				$ardcds  = odbc_result($result1, 'ARDCDS');
				$altcod  = odbc_result($result1, 'ALTCOD');
				$aarumb  = odbc_result($result1, 'AARUMB');
				$auncod  = odbc_result($result1, 'AUNCOD');
				
				$total  += $ardcds;
				
				?>
                <tr >
                    <td style=" border-bottom-style: ridge; border-bottom-width: 1px;"><?php echo $atqdes; ?></td>
                    <td style=" border-bottom-style: ridge; border-bottom-width: 1px;"><?php echo $arqnro; ?></td>
                    <td style=" border-bottom-style: ridge; border-bottom-width: 1px;"><?php echo $afesol; ?></td>
                    <td style=" border-bottom-style: ridge; border-bottom-width: 1px;"><?php echo unisolicitante($auncod, $Compania); ?></td>
                    <td style=" border-bottom-style: ridge; border-bottom-width: 1px;"><?php echo unidad_medidad($aarumb,$Compania,$ardcds); ?></td>
                    <td style=" border-bottom-style: ridge; border-bottom-width: 1px;" align="right"><?php echo number_format(($ardcds),2,",","."); ?></td>
                </tr>
                <?php 
			}
		  ?>
          
          <tr id="confir" name="confir" >
            <td colspan="5" align="right" >&nbsp; <strong>Reserva Final: </strong></td>
            <td align="right"><strong><?php echo number_format(($total),2,",",".");  ?></strong></td>
          </tr>
        <tr>
        <td align="center" colspan="8">
            <br><br>
            <input name="cerrar" type="button" onClick="window.close();" value="Cerrar">
        </td>
      </tr>
    </table>
    </div>
</body>
</html>
