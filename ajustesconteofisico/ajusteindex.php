<?php 
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.1/media/css/demo_page.css";
			@import "../DataTables-1.9.1/media/css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.1/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
				  document.getElementById('info').style.display ="";
				  oTable = $('#info').dataTable({
				  	"iDisplayLength": 25,/*cantidad de pag iniciales*/
					"aLengthMenu": [[25, 50,100, -1], [25, 50,100, "All"]],/*lista de lineas*/
					"fnDrawCallback": function ( oSettings ) {
						if ( oSettings.aiDisplay.length == 0 ){return;}
						 
						var nTrs = $('#info tbody tr');

						var iColspan = nTrs[0].getElementsByTagName('td').length;
						var sLastGroup = "";
						var sLastGroup2 = "";
						for ( var i=0 ; i<nTrs.length ; i++ )
						{
							
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[0];
							if ( sGroup != sLastGroup )
							{
								var nGroup = document.createElement( 'tr' );
								var nCell = document.createElement( 'td' );
								nCell.colSpan = iColspan;
								nCell.className = "Almacen";
								nCell.innerHTML = sGroup;
								nGroup.appendChild( nCell );
								nTrs[i].parentNode.insertBefore( nGroup, nTrs[i] );
								sLastGroup = sGroup;
							}
							
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup2 = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[1];
							if ( sGroup2 != sLastGroup2 )
							{
								var nGroup2 = document.createElement( 'tr' );
								var nCell2 = document.createElement( 'td' );
								nCell2.colSpan = iColspan;
								nCell2.className = "Fecha";
								nCell2.innerHTML = sGroup2;
								nGroup2.appendChild( nCell2 );
								nTrs[i].parentNode.insertBefore( nGroup2, nTrs[i] );
								sLastGroup2 = sGroup2;
							}
							
						}
						
					},
					"aoColumnDefs": [ { "bVisible": false, "aTargets": [ 0 ] }, { "bVisible": false, "aTargets": [ 1 ] } ],
					"aaSortingFixed": [ [ 0, 'asc' ], [ 1, 'desc' ] ],
					"bStateSave": true,
					"oLanguage": 
					{
			          "sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
					  "oPaginate": 
					  {
			            "sNext": "Siguiente",
						"sPrevious": "Anterior"
			          },
					  
			        }
				});
			  } 
			  );
</script>
</head>
<body>
<div id="wrapper">
  <?php 
  		include("../superior.php");
  		?>
  <div id="page">
      <?php include("../validar.php");  		?>

<!--<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Archives</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
      </ul>
	    </div>-->
<!--	  <div id="content" style="background-image: url(fondoimagen.png);">-->
   	  
 		<?php 
		
				/*seccion de carga de informacion*/
		 		$wsolicitud=0;
				/*nuevo*/	
			$sqlsup = "SELECT AUSCIA, AUSCOD, AUSSUP, AUSUSR FROM IDASYSW.mb20fp WHERE AUSCOD ='".$Usuario."' AND AUSCIA='".$Compania."'";
			$resultsup =@odbc_exec($cid,$sqlsup)or die(exit("Error en odbc_exec 11111"));
			$supervisor = odbc_result($resultsup, 'AUSSUP');
			if($supervisor=='S')
			{
				$CodAlm="";
				$CodAlm=trim($_GET['aalcod']);
				if(empty($CodAlm))$CodAlm='0001';
				/*buscar almacen */
				$sqltop = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp t1, iv07fp t2 WHERE       
						T1.ACICOD= T2.ACICOD and T1.AALCOD= T2.AALCOD and t1.ACICOD ='".$Compania."' 
						and T1.AUSCOD = '".$Usuario."' and t1.aalcod= '".trim($CodAlm)."'";
				$resulttop=@odbc_exec($cid,$sqltop)or die(exit("Error en odbc_exec 11111"));
				$listCodAlm = Array();
				$listDesAlm = Array();
				while(odbc_fetch_row($resulttop))
				{
					$listCodAlm[] = odbc_result($resulttop,"AALCOD");
					if(empty($CodAlm)){$CodAlm = odbc_result($resulttop,"AALCOD");}
					$listDesAlm[] =odbc_result($resulttop,"AALDES");
				}
			}
			else
			{
				/*buscar almacen */
				$sqltop = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp t1, iv07fp t2 WHERE       
						T1.ACICOD= T2.ACICOD and T1.AALCOD= T2.AALCOD and t1.ACICOD ='".$Compania."' 
						and T1.AUSCOD = '".$Usuario."' ";
				$resulttop=@odbc_exec($cid,$sqltop)or die(exit("Error en odbc_exec 11111"));
				$listCodAlm = Array();
				$listDesAlm = Array();
				$CodAlm="";
				$DesAlm="";
				while(odbc_fetch_row($resulttop))
				{
					$listCodAlm[] = odbc_result($resulttop,"AALCOD");
					$CodAlm .= odbc_result($resulttop,"AALCOD").",";
					$listDesAlm[] =odbc_result($resulttop,"AALDES");
					$DesAlm .=odbc_result($resulttop,"AALDES").", ";
				}
				if(!empty($CodAlm)){
					$CodAlm = substr($CodAlm,0, (strripos($CodAlm,",")));
				}
				if(!empty($DesAlm)){
					$DesAlm = substr($DesAlm,0,(strripos($DesAlm,',')));
				}
			}
			
						if ($solicitudpagina==0){
						if($CodAlm == ''){
							//echo "<br><br><h1><center>Ud no est� asociado a alguna unidad solicitante</center></h1><br><center><img src='../images/alert.png' alt='Alerta' width='40' height='40' border='0'></center>";
							echo "<br><br><h1><center>Ud no est� asociado a alg�n Almacen</center></h1><br><center><img src='../images/alert.png' alt='Alerta' width='40' height='40' border='0'></center>";
							echo "<br><br><center>Por favor comuniquese con el administrador del sistema</center>";
							echo "<br><h3><center><a href=http://".$Direccionip."/syscv/index.php>REGRESAR</a></center></h3>";
							exit;
						}else{
							foreach ($listCodAlm as $value)	{	$temp .=" or AALSEL like '%".$value."%' ";	}
							$temp = substr($temp, 0, (strlen($temp)-1));
							
							$sql = "SELECT ATJFEC, ATJCTS, AALSEL, ATISEL, ATJNRI, ATJSTS, ATJUSR
										 FROM IV24FP 
										 WHERE ACICOD='$Compania'  and (1=2 $temp)
										 ORDER BY ATJFEC desc";
							  
						}
						//echo $sql;
						
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						$z=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						while(odbc_fetch_row($resultt)){
										
										$jml = odbc_num_fields($resultt);
										$row[$z]["pagina"] =  $pag;
										for($i=1;$i<=$jml;$i++)
										{$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);}
										$z++;
							if ($lin>=$limitep) {$limitep+=$_SESSION['solicitudlineasporpaginat'];
												$pag++;}
							$lin++;
						}
						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
			}
				
			?> 
        <div id="content3" >     
        <table width="100%" border="0">
          <tr>
            <td width="84%" ><h1 align="center" class="title">AJUSTE DE ALMACEN</h1><hr  />
            <div align="center">
                    <!--<h3><?php echo $descrp;	?></h3>
                 <h5><?php 	echo "Almac�n: ".$descrpa	 ?></h5>-->
                 <form><strong>Almacen:</strong>&nbsp;
                
                    <?PHP 
                        if($supervisor =='S') 
                        {?>
                            
                            <select name="aalcod" id="aalcod" onchange="submit();">
                            <?php 
							$CodAlm2 = explode(",", trim($CodAlm));
							$CodAlm3 ='';
							foreach($CodAlm2 as $key => $value)
							{
								$CodAlm3 .= "'".$value."',";
							}
							$CodAlm3 = substr($CodAlm3,0,(strripos($CodAlm3,",")));
                            $sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania' order by AALDES ";
                            $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                            $select = '';
                            while(odbc_fetch_row($result1)){

                                $cod=trim(odbc_result($result1,1));
                                $des=trim(odbc_result($result1,2));
                                if(!empty($CodAlm))
                                {
                                    if($cod == $CodAlm){
                                        $select = ' selected="selected" ';
                                    }
                                    else{
                                        $select = '';
                                    }
                                }											
                            ?>
                                <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des." ( ".$cod." )"; ?></option>
                            <?php } ?>
                        </select>
                        <?php 	
                        }
                        else
                        {
                            echo $DesAlm; 
                        }
                    ?>
                    </form>
                 </div>
            </td>
            <td width="16%" ><div align="left">
              <table width="100%"  border="0">
                <tr>
                  <th width="30%" ><img src="../images/excel.jpg" alt="" width="25" height="25" /></th>
                  <th width="16%" ><img src="../images/pdf.jpg" alt="" width="25" height="25" /></th>
                  <th width="18%" ><img src="../images/impresora.gif" alt="" width="25" height="25" /></th>
                </tr>
              </table>
            </div></td>
          </tr>
        </table>
        
        <div id="container">
            <div id="demo">
            <table width="100%" id="info" style="display:none">
                <thead>
                    <tr>
                    	<th>Almacen</th>
                        <th>Fecha</th>
                        <th align="center">Secuencia</th>
                        <th align="center">Tipo de Inventario</th>
                        <th align="center">Nro de Tarjeta</th>
                        <th align="center">Estatus</th>
                        <th align="center">Usuario</th>
                        <th align="center">Opciones</th>
                    </tr> 
                </thead>
                <tbody >
                	 <?php 
                        $astfld="";
                        /*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
                        $paginat=$_SESSION['solicitudarreglo'];
                        
                        $pagact=$solicitudpagina;
                        for($g=0; $g < (count($paginat)); $g++)
                        {
                     ?>
                    <tr>                
                        <td ><div ><?php echo alamcen($paginat[$g]["AALSEL"], $Compania)."<strong>(".$paginat[$g]["AALSEL"].")</strong>";?></div></td>
                        <td ><div >&nbsp;&nbsp;&nbsp;<?php echo $paginat[$g]["ATJFEC"];?></div></td>
                        <td ><div align="center"><?php echo $sec=$paginat[$g]["ATJCTS"];?></div></td>
                        <td ><div ><?php $invet = new inf_tinventario($cia, ''); echo $invet->nombre_listtinventario($paginat[$g]["ATISEL"], $Compania); //echo $paginat[$g]["ATISEL"];?></div></td>
                        <td ><div ><?php echo $paginat[$g]["ATJNRI"];?></div></td>
                        <td ><div ><?php $sta=$paginat[$g]["ATJSTS"];  echo status('ATJSTS',$paginat[$g]["ATJSTS"]);?></div></td>
                        <td ><div ><?php echo $paginat[$g]["ATJUSR"];?></div></td>
                        <td >
                        	<ul id="opciones" >
                                <li><a href="javascript:verconteofisico(<?php echo "'".trim(fecdma($paginat[$g]["ATJFEC"],"amd","."))."','".$sec."'"; ?>)"><img src="../images/conteo_1_2.png" alt="Conteo Fisico" width="25" height="25" border="0"></a></li>
                                <?php 
								//echo "*-".accesotodasunisol_alma(6)."-*";
									if( (trim($sta) == '05' || trim($sta) == '07' || trim($sta) == '08') && (accesotodasunisol_alma(6)=='S') ){
								?> 
	                                <li><a href="javascript:ajustefisico(<?php echo "'".trim(fecdma($paginat[$g]["ATJFEC"],"amd","."))."','".$sec."'"; ?>)"><img src="../images/ajuste_2.png" alt="Ajuste" name="Ajuste" title="Ajuste" width="25" height="25" border="0"></a></li>
									<li><a href="javascript:anularconteo(<?php echo "'".trim(fecdma($paginat[$g]["ATJFEC"],"amd","."))."','".$sec."'"; ?>)"><img src="../images/rechazado.png" alt="Anular" name="Anular" title="Anular" width="25" height="25" border="0"></a></li>
                                <?php } ?>
                            </ul>
                        </td>
                        </tr>
                        <?php } ?>
                </tbody>
            </table>
	        </div>
        </div>
	  </div>
	</div>
		<!-- end #content -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
</div>
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
