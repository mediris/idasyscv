<?php 
session_start();
include("../conectar.php");
require_once('../tcpdf/config/lang/eng.php');
require_once('../tcpdf/tcpdf.php');

$conteo = trim($_GET['conteo']);

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($Usuarionombre);
$pdf->SetTitle('Conteo fisico '.$conteo.'');
$pdf->SetSubject('Conteo fisico '.$conteo.'');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Despachar Pedido', PDF_HEADER_STRING);
//$pdf->SetHeaderData('../../../../images/logoidacadef20052.png', PDF_HEADER_LOGO_WIDTH, 'Conteo Fisico', 'R.I.F.: '.$Companiarif);
if($Compania=='14')			{	$img = "../../images/logoidacadef20052.png";	}
else if($Compania=='01')	{	$img = "../../images/logomeditronnuevo2.png";	}
else 						{ 	$img='';										}


$pdf->SetHeaderData($img, PDF_HEADER_LOGO_WIDTH, 'Conteo Fisico', 'R.I.F.: '.$Companiarif);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// add a page
$pdf->AddPage();
// set font
$pdf->SetFont('helvetica', '', 10);

	//CABECERA
	$sql="SELECT ATJFEC, ATJCTS, AALSEL, ATISEL, ATJNRI, ATJSTS, ATJUSR, ATJTXT,  ATRCDE, ATRCDS , 
				 AFEIC1, AFEFC1, ASTCT1, AFEIC2, AFEFC2, ASTCT2, AFEICF, AFEFCF, ASTCTF 
		  FROM IV24FP 
		  where ACICOD = '$Compania' AND ATJFEC='$atjfec' AND ATJCTS='$atjcts'";
	
	$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
	
	$feccon = odbc_result($resultt,'ATJFEC') ;
	$descrp = odbc_result($resultt,'ATJTXT');
	$sta24 =(odbc_result($resultt,'ATJSTS'));
	
	
	$secuen =(odbc_result($resultt,'ATJCTS'));
	$listaalsel =(odbc_result($resultt,'AALSEL'));
	$listatisel =(odbc_result($resultt,'ATISEL'));
	$nroini =(odbc_result($resultt,'ATJNRI'));
	$atrcde =(odbc_result($resultt,'ATRCDE'));
	$atrcds =(odbc_result($resultt,'ATRCDS'));
	
	$afeic1 =(odbc_result($resultt,'AFEIC1'));
	$afefc1 =(odbc_result($resultt,'AFEFC1'));
	$astct1 =(odbc_result($resultt,'ASTCT1'));
	$afeic2 =(odbc_result($resultt,'AFEIC2'));
	$afefc2 =(odbc_result($resultt,'AFEFC2'));
	$astct2 =(odbc_result($resultt,'ASTCT2'));
	$afeicf =(odbc_result($resultt,'AFEICf'));
	$afefcf =(odbc_result($resultt,'AFEFCf'));
	$astctf =(odbc_result($resultt,'ASTCTf'));
					
				
				
	
	/*if($sta24 == '01')
	{
		iniciarcarga('02', $sta24, $atjfec, $atjcts, $Compania, $cid, $modulo);
		echo "<script>location.reload();</script>";
	}
	else if($sta24 == '03')
	{
		iniciarcarga('04', $sta24, $atjfec, $atjcts, $Compania, $cid, $modulo);
		echo "<script>location.reload();</script>";
	}*/
	
	
	if ($solicitudpagina==0) 	
	{
		//DETALLE
		$sql="";
		$sql=" SELECT T1.ACICOD, T1.AALCOD, T1.ATJFEC, T1.ATJNRO, T1.ATJEXT, T1.AARCOD, T3.AARDES, T1.AARUMB, T4.AUMDES, T1.ATRLOT, T1.ATJEXI,  T1.ATJCSD, MAX(CASE WHEN T2.ACICOD IS NOT NULL THEN 'S' END ) AS SIHAY 
				FROM IV25FP T1 
						  LEFT JOIN IV26FP T2 ON(T1.ACICOD= T2.ACICOD AND T1.ATJFEC=T2.ATJFEC AND T1.ATJNRO=T2.ATJNRO AND T1.ATJEXT=T2.ATJEXT AND T1.AALCOD= T2.AALCOD AND T2.ATJCON='1'),
					IV05FP T3, IV13FP T4, IV07FP T5 
				WHERE T1.ACICOD= T3.ACICOD AND T1.AARCOD= T3.AARCOD AND 
					T1.AARUMB=T4.AUMCOD and 
					T1.AALCOD=T5.AALCOD AND T1.ACICOD='$Compania' AND 
					t1.atjfec='".fecdma($feccon,'amd','.')."' AND t1.ATJCTS='$atjcts' ";
		if( $sta24 == '04' || $sta24 == '05')
					$sql.=" AND T1.ATJSTD in ( '03', '05', '06' )";
		$sql.= " GROUP BY T1.ACICOD, T1.AALCOD, T1.ATJFEC, T1.ATJCTS, T1.ATJNRO, T1.ATJEXT, T1.AARCOD, T3.AARDES, T1.AARUMB, T4.AUMDES, T1.ATRLOT, T1.ATJEXI, T1.ATJCSD 
				ORDER BY T1.AALCOD, T3.AARDES, T1.ATJNRO ";
		//echo $sql;
	
		$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
		$z=0;
		$lin=1;
		$limitep=$_SESSION['solicitudlineasporpaginat'];
		$pag=1;
		$primero='S';
		
		while(odbc_fetch_row($resultt)){
						
						$jml = odbc_num_fields($resultt);
						$row[$z]["pagina"] =  $pag;
						for($i=1;$i<=$jml;$i++)
						{$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);}
						$z++;
			if ($lin>=$limitep) {$limitep+=$_SESSION['solicitudlineasporpaginat'];
								$pag++;}
			$lin++;}
			$totsol=($lin-1);
			$_SESSION['totalsolicitudes']=$totsol;
			$_SESSION['solicitudarreglo']=$row;
			$solicitudpagina=1;
			$_SESSION['solicitudpaginas']=$pag;
		}	
	

			$html.='<table border="0">
				<tr><td>
					<table width="100%" border="0" class="tabla1">
						  <tr class="tablapasoscss">
							<th height="60" colspan="4" scope="col"><h4>Carga de Conteo Fisico '.$conteo.'</h4></th>
						  </tr>
						  <tr>
							<td colspan="4">
								<table width="100%" border="0" align="left">
								  <tr>
									<td>Fecha de Conteo: '.fecdma($feccon, 'amd','/').'</td>
									<td>N&uacute;mero de Tarjeta Inicial: <strong>'.$nroini.'</strong></td>
								  </tr>
								</table>
							</td>
						  </tr>
						  <tr><td colspan="4"><hr /></td></tr>
						  <tr>
							<td><strong>C&oacute;digo de Almac&eacute;n</strong></td>
							<td><strong>Tipo de Inventario</strong></td>
							<td><strong>Transacci&oacute;n de Entrada:</strong></td>
							<td><strong>Transacci&oacute;n de Salida:</strong></td>
						  </tr>
						  <tr >
							<td>'.$listaalsel.'</td>
							<td>'.$listatisel.'</td>
							<td>'.transaccion($atrcde,$Compania).'</td>
							<td>'.transaccion($atrcds, $Compania).'</td>
						  </tr>
					</table>
				</td></tr>
				<tr><td>
				<tr><td><hr /></td></tr>
				</td>';
				$html.='<tr><td>
							<table border="0" class="tabla1" cellpadding="0">
								<tr>
								  <th style="font-size:8" align="center"><strong>Nro<br /> Tarjeta</strong></th>
								  <th align="center"><strong>C&oacute;digo Art&iacute;culo</strong></th>
								  <th align="center"><strong>Descripci&oacute;n</strong></th>
								  <th align="center"><strong>Conteo</strong></th>
								  <th align="center"><strong>Unidad Medida</strong></th>
								</tr>';

				$paginat=$_SESSION['solicitudarreglo'];
				for($g=0; $g < (count($paginat)); $g++)
				{
					if($g&1==1)
						$bg = '';
					else
						$bg = 'bgcolor="#CCCCCC"';
					//DATOS DEL DETALLE
					$sql3="SELECT AALCOD, ATJFEC, ATJCTS, ATJNRO, ATJEXT, AARCOD, AARUMB, ATRLOT, ATJUBI, ATJCSD, 
								ATJCN1, ASDCT1, ATJCN2, ASDCT2, ATJCNF, ATJSTD 
							FROM iv25fp WHERE ACICOD='".$Compania."' AND ATJFEC='".$atjfec."' and 
							ATJCTS='".$atjcts."' and ATJNRO ='".$paginat[$g]["ATJNRO"]."' and AARCOD = '".$paginat[$g]["AARCOD"]."'";
					$result3=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
			
				$html.='<tr '.$bg.'>
						<td align="right">'.$paginat[$g]["ATJNRO"].'</td>
						<td>'.$paginat[$g]["AARCOD"].'</td>
						<td>'.utf8_encode($paginat[$g]["AARDES"]).'</td>
						<td  align="center">____</td>
						<td>'.unidad_medidad($paginat[$g]["AARUMB"],$Compania,1).'</td>
					</tr>';
				}
				
			$html.='</table>
			</td></tr>
			<tr><td><hr /></td></tr>
			<tr><td>
				<table>
					<tr>
					  <td><strong>Observaciones: </strong></td>
					  <td>'.$descrp.'</td>
				  </tr>
			  </table>
			</td></tr>
			</table>';
	
//	echo $html;
$tbl = <<<EOD
	$html
EOD;

	$pdf->writeHTML($tbl, true, false, false, false, '');
	$pdf->Output('Despacho_'.$atrnum.'.pdf', 'I');

?>