<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl" lang="nl">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="../css/estilos1.css" rel="stylesheet" type="text/css">
  <script language="JavaScript" src="../javascript/javascript.js"></script>
  <script language="JavaScript" src="../javascript/jquery.js"></script>
  <script language="JavaScript" src="../calendario/javascripts.js"></script>
  <title>GABC, Tu Administraci&oacute;n</title>
  <script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
  </script>
  </head>

<body id="index" onload="menuizquierdo('0900000')">
  
<!-- start header -->
<?php include("../superior.php");
?>
<?php include("../validar.php");
?>

<!-- start page -->
<div id="container" style="position:absolute; width:919px; height:262px; z-index:1; left: 1px; top: 124px;">
	<!-- start content -->
	<div id="content" style="position:absolute; width:719px; height:368px; z-index:1; left: 217px; top: 1px;">
		<div class="post">
			<table width="100%"  border="0">
              <tr>
                <td width="76%" scope="col"><h1 align="center" class="title">Tipos de Eventos/Noticias </h1></td>
                <td width="24%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="36%" scope="col"><span class="iconos"><img src="../imagenes/agregar.gif" alt="Agregar" width="25" height="25" onclick="tiponeagregar()"></span></th>
                      <th width="30%" scope="col">&nbsp;</th>
                      <th width="16%" scope="col">&nbsp;</th>
                      <th width="18%" scope="col">&nbsp;</th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>
			
			<div class="entry">
<table id="tablatipone" class="tablapasoscss" width="100%"  border="0">
	
  <thead>
  	<tr>
    	<th width="9%" height="32" scope="col">Tipo</th>
    	<th width="37%" scope="col">Descripci&oacute;n</th>
    	<th width="20%" scope="col">Evento o Noticia </th>
		<th width="10%" scope="col">opciones:</th>
  	</tr>
  </thead>
    <tbody>
	          <?php $sql="Select * from IDASYSW.mb35fp order by atncod ";
				$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
  				while(odbc_fetch_row($result)){			?>
  	<tr>
    	<td scope="col"><div ><strong>
    	  <?php echo odbc_result($result,1);?>
  	  </strong></div></td>                
    	<td scope="col"><?php echo odbc_result($result,2);?></td>
    	<td scope="col"><?php if (odbc_result($result,3)=='N') {echo "Noticia";} else {echo "Evento";};?></td>
		<td valign="top" scope="col">
          <table width="100%" height="100%"  border="0">
            <tr>
              <th width="34" height="17" scope="col"><a href="javascript:tiponeeditar('<?php echo trim(odbc_result($result,1));?>')"><img src="../imagenes/editar.gif" alt="Editar informaci&oacute;n" width="15" height="15" border="0"></a></th>
              <th width="35" scope="col"><a href="javascript:tiponeeliminar('<?php echo trim(odbc_result($result,1));?>')"><img src="../imagenes/eliminar.gif" alt="Eliminar" width="15" height="15" border="0"></a></th>
            </tr>
          </table>  </td>	
          </tr>
			  <?php } ?>
  </tbody>

</table>
		  </div>
	  </div>
	</div>
	<!-- end content -->
	<!-- start sidebar -->
	<!-- end sidebar -->

	<div style="clear: both;">&nbsp;
	  <div id="izquierdo" style="position:absolute; width:200px; height:368px; z-index:1; left: 1px; top: 1px;">
	  	<div id="sidebar">
	  <ul>
			
			<li>
				<h2>Opciones Generales:</h2>
				<div id="opciones">
				</div>
			</li>
      </ul>
	</div>

	  		<?php if ($Usuario!="") { ?> 
<div id="noticiayeventos" >
<iframe src="../noticias/noticiasyeventos.php" width=212px height=117px scrolling="NO" frameborder="0"></iframe>
</div>
 <?php }  ?>
	  </div>
	</div></div>
	<div id="agregartiponediv" class="white_content_tnye">
	  <div>

   
  <form id="tiponeagregarform" name="tiponeagregarform" method="post" action="">
    <fieldset id="form">
    <legend><div id="tituloop"></div></legend>
    <table width="100%"  border="0">
      <tr>
        <th width="20%" scope="col"><label>Tipo Noticia/Evento</label></th>
        <th width="36%" scope="col"><div align="left">
          <input name="atncod" type="text" id="atncod" size="2" maxlength="2" onKeyUp="tiponeagregarform.atncod.value=tiponeagregarform.atncod.value.toUpperCase()">
        </div></th>
        <th colspan="2" id="erratncod" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Descripci&oacute;n</label></th>
        <th scope="col"><div align="left">
          <input name="atndes" type="text" id="atndes" size="40" maxlength="40">
        </div></th>
        <th colspan="2" id="erratndes" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col"><label>Evento/Noticia:</label></th>
        <th scope="col"><div align="left">
          <p>
            <label>
            <input name="atntip" type="radio" value="E">
  Evento</label>

            <label>
            <input type="radio" name="atntip" value="N" checked>
  Noticia</label>
            <br>
          </p>
</div></th>
        <th colspan="2" id="erratntip" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th scope="col">&nbsp;</th>
        <th scope="col"></th>
        <th width="23%" id="erraususr" scope="col"><p onclick="tiponeagregarcerrar()" class="subir" align="right">salir</p></th>
        <th width="21%" id="erraususr" scope="col"><p align="right" class="subir" onclick="tiponeagregarvalidar()">Grabar</p></th>
      </tr>
    </table>
    </fieldset>
  </form>
</div>
</div>
<div id="editartiponediv" class="white_content_tnye">
	  <div>

   
  <form id="tiponeeditarform" name="tiponeeditarform" method="post" action="">
    <fieldset id="form">
    <legend><div id="tituloop1"></div></legend>
    <table width="100%"  border="0">
	
      <tr>
        <th width="20%" scope="col"><label>Tipo Noticia/Evento</label></th>
        <th width="36%" id="wsatncod" align="left" scope="col">
        </th>
        <th colspan="2"  class="Estilo5" scope="col"><div align="left">
          <input name="atncod" type="hidden" id="atncod"></div></th>
      </tr>
      <tr>
        <th scope="col"><label>Descripci&oacute;n</label></th>
        <th scope="col"><div align="left">
          <input name="atndes" type="text" id="atndes" size="40" maxlength="40">
        </div></th>
        <th colspan="2" id="erreatndes" class="Estilo5" scope="col">&nbsp;</th>
      </tr>
	        <tr>
        <th scope="col"><label>Evento/Noticia:</label></th>
        <th scope="col"><div align="left">
          <p>
            <label>
            <input name="atntip" type="radio" value="E">
  Evento</label>

            <label>
            <input type="radio" name="atntip" value="N" checked>
  Noticia</label>
            <br>
          </p>
</div></th>
        <th colspan="2" id="erreatntip" class="Estilo5" scope="col">&nbsp;</th>
      </tr>

      <tr>
        <th scope="col">&nbsp;</th>
        <th scope="col"></th>
        <th width="23%"  scope="col"><p onclick="tiponeeditarcerrar()" class="subir" align="right">salir</p></th>
        <th width="21%"  scope="col"><p align="right" class="subir" onclick="tiponeeditarvalidar()">Modificar</p></th>
      </tr>
    </table>
    </fieldset>
  </form>
</div>
</div>

<div id="fade" class="black_overlay"></div>

<!-- end page -->


  <div id="navigation">
	<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
  </div>
</body>
</html>
