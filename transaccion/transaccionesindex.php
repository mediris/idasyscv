<?php 
/*
 *jDavila
 *30/03/2012
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.1/media/css/demo_page.css";
			@import "../DataTables-1.9.1/media/css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.1/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
				  document.getElementById('info').style.display ="";
			      $('#info').dataTable( 
				  {
					"iDisplayLength": 25,/*cantidad de pag iniciales*/
					"aLengthMenu": [[25, 50,100, -1], [25, 50,100, "All"]],/*lista de lineas*/
			        //inicio seccion de agrupacion
					"fnDrawCallback": function ( oSettings ) {
						if ( oSettings.aiDisplay.length == 0 ){ return;}				 
						var nTrs = $('#info tbody tr');
						var iColspan = nTrs[0].getElementsByTagName('td').length;
						var sLastGroup = "";
						for ( var i=0 ; i<nTrs.length ; i++ )
						{
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[0];
							if ( sGroup != sLastGroup )
							{
								var nGroup = document.createElement( 'tr' );
								var nCell = document.createElement( 'td' );
								nCell.colSpan = iColspan;
								nCell.className = "AALCOD";//el nombre de la comlumna (debe estar de primera..)
								nCell.innerHTML = sGroup;
								nGroup.appendChild( nCell );
								nTrs[i].parentNode.insertBefore( nGroup, nTrs[i] );
								sLastGroup = sGroup;
							}
						}
					},
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					],
					"aaSortingFixed": [[ 0, 'asc' ]],
					"aaSorting": [[ 1, 'asc' ]],
					"sDom": 'lfr<"giveHeight"t>ip',//fin seccion de agrupacion
					
					"bStateSave": true,
					"oLanguage": 
					{
			          "sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
					  "oPaginate": 
					  {
			            "sNext": "Siguiente",
						"sPrevious": "Anterior"
			          },
					  
			        }
			      } );
			  } );
</script>
</head>
<body>
<div id="wrapper">
  <?php 
  		include("../superior.php");
  		?>
  <div id="page">
      <?php include("../validar.php");  		?>

<!--<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Archives</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
      </ul>
	    </div>-->
<!--	  <div id="content" style="background-image: url(fondoimagen.png);">-->
   	  
 		<?php 
				
				$sqlsup = "SELECT AUSCIA, AUSCOD, AUSSUP, AUSUSR FROM IDASYSW.mb20fp WHERE AUSCOD ='".$Usuario."' AND AUSCIA='".$Compania."'";
				$resultsup =@odbc_exec($cid,$sqlsup)or die(exit("Error en odbc_exec 11111"));
				$supervisor = odbc_result($resultsup, 'AUSSUP');
				
				if($supervisor=='S')
				{
					$CodAlm="";
					$CodAlm=$_GET['aalcod'];
					if(empty($CodAlm))$CodAlm=$_POST['aalcod'];
					if(empty($CodAlm))$CodAlm='0001';
					
					/*buscar almacen */
					$sqltop = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp t1, iv07fp t2 WHERE       
							T1.ACICOD= T2.ACICOD and T1.AALCOD= T2.AALCOD and t1.ACICOD ='".$Compania."' 
							and T1.AUSCOD = '".$Usuario."' and t1.aalcod= '".trim($CodAlm)."'";
					$resulttop=@odbc_exec($cid,$sqltop)or die(exit("Error en odbc_exec 11111"));
					$listCodAlm = Array();
					$listDesAlm = Array();
					while(odbc_fetch_row($resulttop))
					{
						$listCodAlm[] = odbc_result($resulttop,"AALCOD");
						if(empty($CodAlm)){$CodAlm = odbc_result($resulttop,"AALCOD");}
						$listDesAlm[] =odbc_result($resulttop,"AALDES");
					}
					
				}
				else
				{
					/*buscar almacen */
					$sqltop = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp t1, iv07fp t2 WHERE       
							T1.ACICOD= T2.ACICOD and T1.AALCOD= T2.AALCOD and t1.ACICOD ='".$Compania."' 
							and T1.AUSCOD = '".$Usuario."' ";
					$resulttop=@odbc_exec($cid,$sqltop)or die(exit("Error en odbc_exec 11111"));
					$listCodAlm = Array();
					$listDesAlm = Array();
					$CodAlm="";
					$DesAlm="";
					while(odbc_fetch_row($resulttop))
					{
						$listCodAlm[] = odbc_result($resulttop,"AALCOD");
						$CodAlm .= odbc_result($resulttop,"AALCOD").",";
						$listDesAlm[] =odbc_result($resulttop,"AALDES");
						$DesAlm .=odbc_result($resulttop,"AALDES").", ";
					}
					if(!empty($CodAlm)){
						$CodAlm = substr($CodAlm,0, (strripos($CodAlm,",")));
					}
					if(!empty($DesAlm)){
						$DesAlm = substr($DesAlm,0,(strripos($DesAlm,',')));
					}
				}
				/*seccion de carga de informacion*/
		 		$wsolicitud=0;
	
				$sql = "SELECT T1.ACICOD, T1.AALCOD, T1.ADPCOD, T1.ATRCOD, T1.ATRNUM,     
						T1.ATRDES, T1.ATRFEC, T1.ATRHOR, T1.ATRMOR, T1.AUSCOD, T1.ATRSTS, 
						T2.ACICOD, T2.AALCOD, T2.AUSCOD as AUSCOD2, T2.AALSTS 
						FROM IV15FP T1 , IV44FP T2 
						WHERE 
							T1.ACICOD=T2.ACICOD AND              
							T1.AALCOD=T2.AALCOD AND 
							T1.ACICOD ='$Compania' AND ";
				//if($supervisor!='S')$sql .= " T1.AUSCOD='".$Usuario."' AND T2.AUSCOD='".$Usuario."' AND ";
				if($supervisor!='S')$sql .= " T1.AUSCOD IN (T2.AUSCOD) AND ";
				if($supervisor=='S')$sql .= " (T2.AUSCOD='".$Usuario."') AND ";
				$CodAlm2 = explode(",", trim($CodAlm));
				$CodAlm3 ='';
				foreach($CodAlm2 as $key => $value)
				{
					$CodAlm3 .= "'".$value."',";
				}
				$CodAlm3 = substr($CodAlm3,0,(strripos($CodAlm3,",")));
				$sql .= "	T1.AALCOD in (".trim($CodAlm3)." )
						ORDER BY T1.ATRCOD,T1.ATRNUM";

				//echo $sql;
				
				$resultt=@odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				$z=0;
				$lin=1;
				$limitep=$_SESSION['solicitudlineasporpaginat'];
				$pag=1;
				$primero='S';
				
				while(odbc_fetch_row($resultt))
				{
					$jml = odbc_num_fields($resultt);
					$row[$z]["pagina"] =  $pag;
					for($i=1;$i<=$jml;$i++)
					{
						$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
					}
					$z++;
					if ($lin>=$limitep) 
					{
						$limitep+=$_SESSION['solicitudlineasporpaginat'];
						$pag++;
					}
					$lin++;
				}
				/*guarla informacion en un arreglo*/
				$_SESSION['solicitudarreglo']=$row;
				
				/*permite mostrar pag seleccionada*/
				if ($solicitudpagina==0) 	
				{
					$totsol=($lin-1);
					$_SESSION['totalsolicitudes']=$totsol;
					$solicitudpagina=1;
					$_SESSION['solicitudpaginas']=$pag;
				}
				
			?>      
		<div id="content3" >
        	<table width="100%"   border="0">
              <tr>
                <td width="84%" scope="col"><h1 align="center" class="title">TRANSACCIONES:</h1> 
                    <div align="center"><form><strong>Almacen:</strong>&nbsp;
                
                    <?PHP 
                        if($supervisor =='S') 
                        {?>
                            
                            <select name="aalcod" id="aalcod" onchange="submit();">
                            <?php 
							$CodAlm2 = explode(",", trim($CodAlm));
							$CodAlm3 ='';
							foreach($CodAlm2 as $key => $value)
							{
								$CodAlm3 .= "'".$value."',";
							}
							$CodAlm3 = substr($CodAlm3,0,(strripos($CodAlm3,",")));
                            $sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania' order by AALDES ";
                            $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                            $select = '';
                            while(odbc_fetch_row($result1)){

                                $cod=trim(odbc_result($result1,1));
                                $des=trim(odbc_result($result1,2));
                                if(!empty($CodAlm))
                                {
                                    if($cod == $CodAlm){
                                        $select = ' selected="selected" ';
                                    }
                                    else{
                                        $select = '';
                                    }
                                }											
                            ?>
                                <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des." ( ".$cod." )"; ?></option>
                            <?php } ?>
                        </select>
                        <?php 	
                        }
                        else
                        {
                            echo $DesAlm; 
                        }
                    ?>
                    </form></div><hr />
                    </td>
                <td width="16%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                    <?php if(validarAlmacenes(trim($CodAlm), $listCodAlm)){ ?>
                      <th width="36%" scope="col"><a rel="shadowbox;width=850;height=450" title="Agregar Transacciones" href="transaccionesfagregar.php?almacen=<?php echo $CodAlm; ?>"><img src="../images/agregar.gif" alt="Agregar" width="25" height="25" border="0" /></a></th>
                    <?php }else{ ?>
                      <th width="36%" scope="col">&nbsp;</th>
                    <?php } ?>
                      <th width="30%" scope="col"><a href="exportaraexcel.php?aalcod=<?php echo $CodAlm3; ?>&"><img src="../images/excel.jpg" alt="" width="25" height="25" /></a></th>
                      <th width="16%" scope="col"><img src="../images/pdf.jpg" alt="" width="25" height="25" /></th>
                      <th width="18%" scope="col"><img src="../images/impresora.gif" alt="" width="25" height="25" /></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>
         <div id="container">
            <div id="demo">
            <table width="100%" id="info" style="display:none">
                
                    <thead>
                        <tr>
                        	<th >AALCOD</th>
                            <th >C�digo</th>
                            <th >N&uacute;mero</th>
                            <th >Descripci&oacute;n</th>
                            <th >Fecha</th>
                            <th >M&oacute;dulo</th>
                            <th >Usuario</th>
                            <th >Status</th>
                            <th >Opciones</th>
                        </tr> 
                    </thead>
                    <tbody >
                        <?php 
                            $astfld="";
                            /*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
                            $paginat=$_SESSION['solicitudarreglo'];
                            
                            $pagact=$solicitudpagina;
                            for($g=0; $g < (count($paginat)); $g++)
                            {                                
                        ?>
                                 <!--<tr>
                                    <td colspan="6"><?php print_r($paginat[$g]) ?></td>
                                 </tr>-->
                                <tr>
                                	<td scope="col"><div ><strong><?php echo alamcen($paginat[$g]["AALCOD"],$Compania);?></strong></div></td>
                                    <td scope="col"><div align="center"><strong><?php echo transaccion($paginat[$g]["ATRCOD"],$Compania);?></strong></div></td>                
                                    <td scope="col"><div align="left"><?php echo $paginat[$g]["ATRNUM"];?></div></td>
                                    <td scope="col"><div align="left"><?php echo $paginat[$g]["ATRDES"];?></div></td>
                                    <td scope="col"><div align="center"><?php echo $paginat[$g]["ATRFEC"]."-".$paginat[$g]["ATRHOR"];?></div></td>
                                    <td scope="col"><div align="center"><?php echo $paginat[$g]["ATRMOR"];?></div></td>                
                                    <td scope="col"><div align="left"><?php echo $paginat[$g]["AUSCOD"];?></div></td>
                                    <td scope="col"><div align="center"><?php echo status('ATRSTS',$paginat[$g]["ATRSTS"]);?></div></td>
    
                                    <?php /* seccion de opciones */ ?>
                                    <td scope="col">
                                     <ul id="opciones">
                                    <?php if( (validarAlmacenes(trim($CodAlm), $listCodAlm))){ 
											if($paginat[$g]["AUSCOD"]== $paginat[$g]["AUSCOD2"] ||($paginat[$g]["AUSCOD"]='IDASYS' || $paginat[$g]["AUSCOD"]='JDAVILA'))
											{
									?>
                                               		<?php if($paginat[$g]["ATRSTS"]=='01'){?>
                                                    	<li><a title="Editar Transaccion" rel="shadowbox;width=850;height=450"  href="transaccionesfeditar.php?tipo=<?php echo trim($paginat[$g]["ATRCOD"]);?>&num=<?php echo trim($paginat[$g]["ATRNUM"]);?>&almacen=<?php echo trim($paginat[$g]["AALCOD"]); ?>"><img src="../images/editar.gif" alt="Editar" width="15" height="15" border="0" /></a></li>
	                                                    <li><a title="Eliminar" href="javascript:eliminar('<?php echo trim($paginat[$g]["ATRCOD"]);?>','<?php echo trim($paginat[$g]["ATRNUM"]);?>')"><img src="../images/eliminar.gif" alt="Eliminar" width="15" height="15" border="0" /></a></li>
                                                    <?php } ?>
                                                    <?php if($paginat[$g]["ATRSTS"]=='01'){?>
                                                        <li><a title="Confirmar Transaccion" rel="shadowbox;width=650;height=345"  href="confirmar.php?tipo=<?php echo trim($paginat[$g]["ATRCOD"]);?>&num=<?php echo trim($paginat[$g]["ATRNUM"]);?>&almacen=<?php echo trim($paginat[$g]["AALCOD"]); ?>"><img src="../images/check.gif" alt="Editar" width="15" height="15" border="0" /></a></li>
                                                   <?php }} ?>
                                    <?php 	
										  }else{ ?>
                                          &nbsp;
                                          
                                    <?php } ?>  
                                    	<li><a href="javascript:transaccionver('<?php echo trim($paginat[$g]["ATRCOD"]);?>','<?php echo trim($paginat[$g]["ATRNUM"]);?>','<?php echo trim($paginat[$g]["AALCOD"]); ?>')"><img src="../images/ver.png" title="Ver Detalle" width="31" height="28" border="0"></a></li>
                                    </ul>
                                    </td>             
                                </tr>
                        <?php } ?> 
                    </tbody>
                </table>
            </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
</div>
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
