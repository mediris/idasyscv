<?php
/*
 * jDavila
 * 11/04/12
 */
session_start();
header("Content-type: text/javascript; charset=iso-8859-1"); 
include("../conectar.php");
include("../JSON.php");

	$codigo=trim($cadena['code']);
	$num=trim($cadena['num']);
	
	if(empty($num) || trim($num)=='')
	{
		$sql ="SELECT T1.ACICOD, T1.ATRCOD, T1.AMDCOD, T1.APDCOD, T2.APDDES, T2.APDSEC, T2.APDLON, T2.APDDCR, T2.APDTIP, 
				T2.APDVAL, T2.ATACOD, T2.ASBCOD, T2.APDSTS, T2.APDLND, T3.ASBDES, T3.ASBSTS
			 FROM IV37FP T1, MB03FP T2, MB10FP T3 
			 WHERE T1.ACICOD='$Compania' AND T1.APDCOD=T2.APDCOD AND 
			 	T1.AMDCOD=T2.AMDCOD AND T1.AMDCOD='$modulo' AND 
				T2.ATACOD=T3.ATACOD AND T2.ASBCOD=T3.ASBCOD AND 
				T1.ATRCOD='$codigo' 
			 ORDER BY T2.ASBCOD, T2.APDSEC, T1.APDCOD";
	}
	else if(!empty($num))
	{
		$sql = "SELECT T1.ACICOD, T1.ATRCOD, T1.AMDCOD, T1.APDCOD, T2.APDDES,     
						T2.APDSEC, T2.APDLON, T2.APDDCR, T2.APDTIP, T2.APDVAL, T2.ATACOD, 
						T2.ASBCOD, T2.APDSTS, T2.APDLND, T3.ASBDES, T3.ASBSTS, T4.APACOD, 
						T4.ATRNUM, T4.AAPVLA, T4.AAPDES, T4.AAPLON, T4.AAPLND, T4.AAPSEC, 
						T4.AAPTIP 
				FROM mb10fp T3, MB03FP T2, 
					iv37FP T1
						LEFT JOIN IV38FP T4 ON ( T1.ACICOD=T4.ACICOD AND T4.ATRNUM=$num AND T1.ATRCOD=T4.ATRCOD AND T1.APDCOD=T4.APACOD)                                          
				WHERE T1.ACICOD='$Compania' AND T1.APDCOD=T2.APDCOD AND                  
					T1.AMDCOD=T2.AMDCOD AND T1.AMDCOD='$modulo' AND 
					T2.ATACOD=T3.ATACOD  AND T2.ASBCOD=T3.ASBCOD AND 
					T1.ATRCOD='$codigo' 
				ORDER BY T2.ASBCOD, T2.APDSEC, T1.APDCOD";
	}
		
	echo fparam_adi_add_ver_transaccion('agregarform','detalle',$sql);
?>