/*
jDavila
20/03/12
*/
function agregar()
{
	document.form.atrcod.value="";
	document.form.atrdes.value="";
	//document.form.attcod.value="";
	document.form.atrsig.value="";
	document.form.atrmor.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
20/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "transaccionagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
21/03/12
*/
function editar(ttrans, tipo){

	var param = [];
	param['atrcod']=tipo;
	param['attcod']=ttrans;
	ejecutasqlp("transaccioninformacionphp.php",param);

	for(i in gdata)
	{
		document.form.hatrcod.value=gdata[i].ATRCOD;
		document.getElementById("wsatrcod").innerHTML=gdata[i].ATRCOD;
		document.form.atrdes.value=gdata[i].ATRDES;
		document.form.attcod.value=gdata[i].ATTCOD;
		document.form.atrsig.value=gdata[i].ATRSIG;
		document.form.atrmor.value=gdata[i].ATRMOR;
		/*falta cargar los parametros*/
		
	};
	$("#aftsav").hide(1);
}

/*
jDavila
21/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "transaccioneditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
20/03/12
*/
function eliminar(ttrans, tipo) {
	if (confirm('Seguro que desea borrar la transacción ' + tipo + '?'))
	{
		var param = [];
		param['attcod']=ttrans;
		param['atrcod']=tipo;
		ejecutasqld("transaccioneliminar.php",param);
		location.reload();
	}
}

/**/
function buscarparametrosa(tipo){
		var x;
 		x=$("#pa");
  		x.empty();
		ejecutasql("buscarparametrosaphp.php",tipo);
		for(i in gdata)
		{
			var ul = $("#pa");
			var check= (gdata[i].APDTIP!="*") ? " checked": " ";
			ul.append("<li id='pr"+gdata[i].APDCOD+"'><input type='checkbox' name='parama["+i+"]' value='"+gdata[i].APDCOD+"'"+check+"/>"+gdata[i].APDDES+"<ul id='ul"+gdata[i].APDCOD+"'></ul></li>");
		}
}

function agregarusu()
{
	document.agregarform.auscod.value="";

	$("#agregaraftsav").hide(1);
}


function agregarvalidarsus(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "transaccionusuarioagregarvalidar.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}


function eliminarsus(id, atrcod, atrdes) {
	if (confirm('Seguro que desea borrar el usuario ' + id + '?'))
	{
		var param = [];
		param['auscod']=id;
		param['atrcod']=atrcod;
		param['atrdes']=atrdes;
		
		ejecutasqld("transaccionusuarioeliminar.php",param);
		location.reload();
	}
}


