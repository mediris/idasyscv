/*
jDavila
20/03/12
*/
function agregar()
{
	document.form.attcod.value="";
	document.form.attdes.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
20/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "tipotransagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
20/03/12
*/
function editar(tipo){

	var param = [];
	param['attcod']=tipo;
	ejecutasqlp("tipotransinformacionphp.php",param);

	for(i in gdata)
	{
		document.form.attcod.value=gdata[i].ATTCOD;
		document.getElementById("wsattcod").innerHTML=gdata[i].ATTCOD;
		document.form.attdes.value=gdata[i].ATTDES;
	};
	$("#aftsav").hide(1);
}

/*
jDavila
20/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "tipotranseditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
20/03/12
*/
function eliminar(tipo) {
	if (confirm('Seguro que desea borrar tipo de transacción ' + tipo + '?'))
	{
		var param = [];
		param['attcod']=tipo;
		ejecutasqld("tipotranseliminar.php",param);
		location.reload();
	}
}
