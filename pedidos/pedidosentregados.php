<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({
overlayOpacity: "0.5"
});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.1/media/css/demo_page.css";
			@import "../DataTables-1.9.1/media/css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.1/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
				  document.getElementById('info').style.display ="";
			      $('#info').dataTable( 
				  {
			        //inicio seccion de agrupacion
					"fnDrawCallback": function ( oSettings ) {
						if ( oSettings.aiDisplay.length == 0 )
						{
							return;
						}
				 
						var nTrs = $('#info tbody tr');
						var iColspan = nTrs[0].getElementsByTagName('td').length;
						var sLastGroup = "";
						var sLastGroup2 = "";
						/*grupo de status*/
						for ( var i=0 ; i<nTrs.length ; i++ )
						{
							var iDisplayIndex = oSettings._iDisplayStart + i;
							var sGroup = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[0];
							if ( sGroup != sLastGroup )
							{
								var nGroup = document.createElement( 'tr' );
								var nCell = document.createElement( 'td' );
								nCell.colSpan = iColspan;
								nCell.className = "ARQSTS";//el nombre de la comlumna (debe estar de primera..)
								nCell.innerHTML = sGroup;
								nGroup.appendChild( nCell );
								nTrs[i].parentNode.insertBefore( nGroup, nTrs[i] );
								sLastGroup = sGroup;
							}
							
						}
						/*subgrupo de usuario*/
						for ( var d=0 ; d<nTrs.length ; d++ )
						{
							var iDisplayIndex2 = oSettings._iDisplayStart + d;
							var sGroup2 = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex2] ]._aData[1];
							if ( sGroup2 != sLastGroup2 )
							{
								var nGroup2 = document.createElement( 'tr' );
								var nCell2 = document.createElement( 'td' );
								nCell2.colSpan = iColspan;
								nCell2.className = "AUNCOD2";//el nombre de la comlumna (debe estar de primera..)
								nCell2.innerHTML = sGroup2;
								nGroup2.appendChild( nCell2 );
								nTrs[d].parentNode.insertBefore( nGroup2, nTrs[d] );
								sLastGroup2 = sGroup2;
							}
						}
					},
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 1 ] },
						{ "bVisible": false, "aTargets": [ 0 ] }
					],
					"aaSortingFixed": [[ 0, 'asc' ]],
					"aaSorting": [[ 1, 'asc' ]],
					"sDom": 'lfr<"giveHeight"t>ip',
					//fin seccion de agrupacion
					
					"bStateSave": true,
					"oLanguage": 
					{
			          "sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
					  "oPaginate": 
					  {
			            "sNext": "Siguiente",
						"sPrevious": "Anterior"
			          },
					  
			        }
			      } );
			  } );
</script>
</head>
<body>
<div id="wrapper">
  <?php 
  		include("../superior.php");
  		?>
  <div id="page">
      <?php include("../validar.php");  		?>

<!--<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
	      </li>
      </ul>
  </div>-->
	  
         <?php $wsolicitud=0;
				if ($solicitudpagina==0) 	{
					
							$sql="SELECT T1.ARQNRO, T1.AFESOL, T1.AHRSOL, T1.ARQTIP, t2.atqdes, T1.AUNCOD, T1.ARQOBS, T1.ARQSTS, T1.AUSCOD, T1.AUNCOD, T3.AUNDES, T3.AUNSUP 
								FROM is04fp t1, is02fp t2, is01fp t3 
								WHERE T1.ACICOD=T2.ACICOD and T1.ARQTIP=T2.ATQCOD and t1.ACICOD='$Compania' AND t1.arqsts in('06','07','08') and T1.ACICOD=T3.ACICOD and T1.AUNCOD=T3.AUNCOD 
								ORDER BY T1.AUNCOD, t1.arqsts, T1.AFESOL desc, T1.AHRSOL desc ";
						
							$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
							$z=0;
							$lin=1;
							$limitep=$_SESSION['solicitudlineasporpaginat'];
							$pag=1;
							$primero='S';
							
  							while(odbc_fetch_row($resultt)){
											
											$jml = odbc_num_fields($resultt);
											$row[$z]["pagina"] =  $pag;
   			 								for($i=1;$i<=$jml;$i++)
        									{$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);}
											$z++;
								if ($lin>=$limitep) {$limitep+=$_SESSION['solicitudlineasporpaginat'];
													$pag++;}
								$lin++;}
								$totsol=($lin-1);
								$_SESSION['totalsolicitudes']=$totsol;
								$_SESSION['solicitudarreglo']=$row;
							    $solicitudpagina=1;
								$_SESSION['solicitudpaginas']=$pag;
								}
?>
        <div id="content3" >
        	<table width="100%"   border="0">
              <tr>
                <td width="84%" ><h1 align="center" class="title">PEDIDOS ENTREGADOS</h1>
                  <hr /></td>
                <td width="16%" ><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" ><img src="../images/excel.jpg" alt="" width="25" height="25" /></th>
                      <th width="16%" ><img src="../images/pdf.jpg" alt="" width="25" height="25" /></th>
                      <th width="18%" ><img src="../images/impresora.gif" alt="" width="25" height="25" /></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>
			<div id="container">
            <div id="demo">
            <table width="100%" id="info" style="display:none">
            <thead>
              <tr>
              	<th>ARQSTS</th>
                <th>AUNCOD2</th>
                <th width="11%" >N&uacute;mero Pedido</th>
               	<th width="11%" >Usuario Solicitante</th>
                <th width="12%" >Usuario Supervisor</th>
                <th width="39%" >Tipo de Solicitud</th>
                <th width="12%" >Fecha Inicio</th>
                <th width="15%" >Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php $tipopar="";
			  		$tipotec="";
				
				$paginat=$_SESSION['solicitudarreglo'];
				//print_r($paginat);
				$pagact=$solicitudpagina;
				for($g=0; $g < (count($paginat)); $g++){
				?>
              <tr>
              	<td ><div align="left"><strong>Status : <?php echo status('ARQSTS',$paginat[$g]["ARQSTS"]);?></strong></div></td>
                <td ><div align="left">&nbsp;&nbsp;&nbsp;<strong>Unidad Solicitante : <?php echo $paginat[$g]["AUNDES"];?></strong></div></td>
                <td ><div align="center"><strong><?php echo $paginat[$g]["ARQNRO"];?></strong></div></td>
                <td ><div align="center"><?php echo $paginat[$g]["AUSCOD"];?></div></td>
                <td ><div align="center"><?php echo $paginat[$g]["AUNSUP"];?></div></td>
                <td ><div align="center"><?php echo $paginat[$g]["ATQDES"];?></div></td>
                <td ><div align="center"><?php echo $paginat[$g]["AFESOL"]." / ".$paginat[$g]["AHRSOL"];?></div></td>
                <td valign="top" >
                	<ul id="opciones">
                        <li><a href="javascript:requerimverdetalle('<?php echo trim($paginat[$g]["ARQNRO"]);?>')"><img src="../images/ver.png" title="Ver Detalle" width="35" height="35" border="0"></a></li>
                        <?php if($paginat[$g]["ARQSTS"]=='05'){?>
                            <li><a href="javascript:detalleimprimirpdf('<?php echo trim($paginat[$g]["ARQNRO"]);?>')"><img src="../images/impresora.gif" alt="Imprimir informaci&oacute;n" width="30" height="25" border="0" /></a></li>
                        <?php } ?>
                   </ul>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
	    </div>
	  </div>
  </div>
		<!-- end #content -->
		<!-- end #sidebar -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
