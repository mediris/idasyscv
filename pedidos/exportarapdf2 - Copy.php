<?php 
session_start();
include("../conectar.php");
require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');



// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Johan Davila');
$pdf->SetTitle('Despacho Pedido '.$arqnro.'');
$pdf->SetSubject('Despacho Pedido '.$arqnro.'');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Despachar Pedido', PDF_HEADER_STRING);
//$pdf->SetHeaderData('../../../images/logoidacadef20052.png', PDF_HEADER_LOGO_WIDTH, 'Despachar Pedido', 'R.I.F.: '.$Companiarif);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);



// ---------------------------------------------------------

			 $arqnro = trim($_GET["num"]);
			 //$sql2="SELECT T1.ARQNRO, T2.AUNCOD, T2.AUNDES, T1.AFESOL, T1.AHRSOL, T5.ATQDES, T1.ARQOBS, T1.AUSCOD, T3.AUSAP1, T3.AUSAP2, T3.AUSNO1, T3.AUSNO2, T4.AEMMAI, T1.ARQSTS, T2.AUNUBI, T2.AUNTLF FROM is04fp t1, is01fp t2, is02fp t5, IDASYSW.mb20fp  t3 left join IDASYSW.mb21fp t4 on (t3.auscod=t4.auscod and t4.aemprd='S')  WHERE T1.ACICOD= T2.ACICOD and T1.AUNCOD= T2.AUNCOD and T1.AUSCOD= T3.AUSCOD and t1.arqtip= T5.ATQCOD and t1.arqnro=$arqnro and t1.acicod='$Compania'";
			 $sql2="SELECT T1.ARQNRO, T2.AUNCOD, T2.AUNDES, T1.AFESOL, T1.AHRSOL, T5.ATQDES, T1.ARQOBS, T1.AUSCOD, T3.AUSAP1, T3.AUSAP2, T3.AUSNO1, T3.AUSNO2, T4.AEMMAI, T1.ARQSTS, T2.AUNUBI, T2.AUNTLF, T6.AUSCOD AS AUSDEP, T7.AUSCOD AS AUSREC FROM is04fp t1 left join is10fp t6 on (t1.acicod=t6.acicod and  T1.ARQNRO=T6.ARQNRO and  T6.ARQSTS='05') left join is10fp t7 on (t1.acicod=t7.acicod and T1.ARQNRO=T7.ARQNRO and T7.ARQSTS=T1.ARQSTS AND T7.ARQSTS IN ('06', '07', '08') ), is01fp t2, is02fp t5, IDASYSW.mb20fp  t3 left join IDASYSW.mb21fp t4 on (t3.auscod=t4.auscod and t4.aemprd='S')  WHERE T1.ACICOD= T2.ACICOD and T1.AUNCOD= T2.AUNCOD and T1.AUSCOD= T3.AUSCOD and t1.arqtip= T5.ATQCOD and T1.ACICOD= T5.ACICOD and t1.arqnro=$arqnro and t1.acicod='$Compania'";

			$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
			$auncod=trim(odbc_result($result2,2));
			$auscod=trim(odbc_result($result2,8));//usuario quien realizo la ord
			$atqdes=trim(odbc_result($result2,6));
			$aundes=trim(odbc_result($result2,3));
			$afesol=trim(odbc_result($result2,4));
			$ahrsol=trim(odbc_result($result2,5));
			$arqobs=trim(odbc_result($result2,7));
			$aemmai=trim(odbc_result($result2,13));//destinatario
			$arqsts=trim(odbc_result($result2,14));
			$aunubi=trim(odbc_result($result2,15));//ubicacion unid. solicitante
			$auntlf=trim(odbc_result($result2,16));//telefono unid. solicitante
			$aundesp=trim(odbc_result($result2,17));//usuario despachador
			$aunrecp=trim(odbc_result($result2,18));//usuario receptor segun status
			if ($Compania=='01') 	{$logo="../../../images/logomeditronnuevo.png";}
			else					{$logo="../../../images/logoidacadef20052.png";}
			if($arqsts=='06' || $arqsts=='07' || $arqsts=='08')
			{
				$pdf->SetHeaderData($logo, PDF_HEADER_LOGO_WIDTH, 'Informe Recepción de Pedido', 'R.I.F.: '.$Companiarif);
			}
			else
			{
				$pdf->SetHeaderData($logo, PDF_HEADER_LOGO_WIDTH, 'Despachar Pedido', 'R.I.F.: '.$Companiarif);
			}
			// add a page
$pdf->AddPage();
// set font
$pdf->SetFont('helvetica', '', 10);
			
			//detalle del Requerimiento			
			$sqla="SELECT t1.ArdCAN, t1.AARUMb, t2.AARDES,t1.aarcod, t1.ARDCDS, T1.ARDCRC , T1.ARDOBS , T1.ARDCR2 FROM is05FP t1 left join IV05FP t2 on (t1.aarcod=t2.aarcod) WHERE t1.ACICOD='$Compania' and  t1.acicod=t2.acicod and t1.arqnro=$arqnro";
								$resulta=odbc_exec($cid,$sqla)or die(exit("Error en odbc_exec 11111"));
								
			//movimientos de status		
			$sqla="SELECT ARQSTS, ASTFEC, ASTHOR, AUSCOD FROM is10fp WHERE ACICOD ='$Compania' and ARQNRO =$arqnro";
								$results=odbc_exec($cid,$sqla)or die(exit("Error en odbc_exec 11111"));
			
						
			//buscar el supervisor del tecnico
			//$sqlb="SELECT T2.AUNSUP, T3.AUSAP1, T3.AUSAP2, T3.AUSNO1,  T3.AUSNO2, T4.AEMMAI FROM is01fp t2, IDASYSW.mb20fp t3 left join IDASYSW.mb21fp t4 on (t3.auscod=t4.auscod and t4.aemprd='S') WHERE T2.AUNSUP= T3.AUSCOD ";
  			  $sqlb="SELECT T2.AUNSUP, T3.AUSAP1, T3.AUSAP2, T3.AUSNO1,  T3.AUSNO2, T4.AEMMAI FROM is01fp t2, IDASYSW.mb20fp t3 left join IDASYSW.mb21fp t4 on (t3.auscod=t4.auscod and t4.aemprd='S') WHERE T2.AUNSUP= T3.AUSCOD and T2.AUNCOD = '$auncod'";
			$resultb=odbc_exec($cid,$sqlb)or die(exit("Error en odbc_exec 11111"));
			$aunsup=trim(odbc_result($resultb,1));
			$ausno1=trim(odbc_result($resultb,4));
			$ausap1=trim(odbc_result($resultb,2));
			$correosup=trim(odbc_result($resultb,6));
  		
			
$html.='<table width="100%" cellpadding="0" cellspacing="3">
			<tbody class="top">
				<tr>
					<td colspan="4">Tipo de Pedido: '.trim(utf8_encode($atqdes)).'&nbsp;&nbsp;&nbsp;N&uacute;mero:<strong>'.$arqnro.'</strong>
					</td>
					<td colspan="2">
						Fecha: <strong>'.date('d.m.y').'</strong>
					</td>
				</tr>
				<tr >
					<!--<td align="left" valign="middle"><strong>Fecha / Hora:</strong></td>-->
					<td align="left" valign="middle" colspan="2"><strong>Unidad Solicitante:</strong></td>
					<td align="left" valign="middle"><strong>Tel&eacute;fono</strong></td>
					<td align="left" valign="middle"><strong>Usuario:</strong></td>
					<td align="left" valign="middle"><strong>Supervisor:</strong></td>
					<td align="left" valign="middle"><strong>Status:</strong></td>
				</tr>
				<tr >
					<!--<td height="30" align="left" valign="middle">'.$afesol.' / '.$ahrsol.'</td>-->
					<td align="left" valign="middle" colspan="2">'.trim(utf8_encode($aundes)).'</td>
					<td align="left" valign="middle">'.$auntlf.'</td>
					<td align="left" valign="middle"><div>'.trim(utf8_encode(usuario($auscod))).'</div></td>
					<td align="left" valign="middle"><div>'.trim(utf8_encode(usuario($aunsup))).'</div></td>
					<td align="left" valign="middle">'.trim(utf8_encode(status('ARQSTS',$arqsts))).'</td>
				</tr>
				<tr >
					<td align="left" valign="middle"><strong>Direcci&oacute;n:</strong></td>
					<td align="left" valign="middle" colspan="5">'.trim(utf8_encode($aunubi)).'</td>
				</tr>
			</tbody>
	
				<tr>
					<td colspan="6"><hr /></td>
				</tr>
				
			  
			<tbody class="detalle">
			<tr><td colspan="7">
				<table width="100%" border="0" cellspacing="3">
					<tr class="thead">
					<th width="2%"><strong>N°</strong></th>';
			if($arqsts=='06'||$arqsts=='07' || $arqsts=='08'){
				if($arqsts=='08'){$width = 'width="35%"';}else{$width = 'width="45%"';}
				$html.='<th '.$width.' ><strong>Descripci&oacute;n</strong></th>
						<th width="11%"><strong>Cantidad Pedida</strong></th>
						<th width="10%"><strong>Cantidad Despachar</strong></th>';
				$html.='<th width="10%"><strong>Cantidad Recibida</strong></th>';
				if($arqsts=='08'){
					$html.='<th width="10%"><strong>Cantidad Recibida 2</strong></th>';
				}
				$html.='<th width="10%"><strong>Unidad de<br />Medida</strong></th>';
				$html.='<th width="14%"><strong>Observacion</strong></th>';
			}
			else
			{
				$html.='<th width="50%"><strong>Descripci&oacute;n</strong></th>
						<th width="11%"><strong>Cant. Pedida</strong></th>
						<th width="16%"><strong>Cant. a Despachar</strong></th>';
				$html.='<th width="11%"><strong>Unidad de<br />Medida</strong></th>';
				$html.='<th width="10%" align="center"><strong>Recibida</strong></th>';
			}
			
			$html.='</tr>';

					$uno='N';
					$can =0;
                    while(odbc_fetch_row($resulta)){
						$can++;
						$aarcod=trim(odbc_result($resulta,4));	
						if ($uno!='S') {$campoinit='ardcds['.$aarcod.']';
										$uno='S';};
						$descripcion=odbc_result($resulta,3);
						$cantidad=number_format(odbc_result($resulta,1),2,",",".");
						if($arqsts=='06'||$arqsts=='07' || $arqsts=='08'){//T1.ARDCRC , T1.ARDOBS
							if($arqsts=='06'||$arqsts=='07'||$arqsts=='08'){
								$ardcrc=number_format(odbc_result($resulta,'ARDCRC'),2,",",".");
							}
							if($arqsts=='08'){
								$ardcrc2=number_format((odbc_result($resulta,'ARDCR2')),2,",",".");
							}
							$ardobs=odbc_result($resulta,'ARDOBS');
						}
						/*if (odbc_result($resulta,5)==0) {$cantidaddes=' ';}
						else							*/{$cantidaddes=number_format(odbc_result($resulta,5),2,",",".");}
						$unidad=odbc_result($resulta,2);


						$html.='
							<tr>
								<td valign="top" >'.$can.'</td>';
						
								if($arqsts=='06'||$arqsts=='07' || $arqsts=='08')
								{
									$html.='<td valign="top" >'.trim(utf8_encode($descripcion)).'<strong>('.$aarcod.')</strong></td>								
											<td valign="top" align="right">'.$cantidad.'&nbsp;&nbsp;</td>
											<td valign="top" align="right">'.$cantidaddes.'&nbsp;&nbsp;</td>';
									$html.='<td valign="top" align="right">'.$ardcrc.'&nbsp;&nbsp;</td>';
									if($arqsts=='08'){
										$html.='<td valign="top" align="right">'.$ardcrc2.'&nbsp;&nbsp;</td>';
									}
									$html.='<td valign="top">'.trim(utf8_encode(unidad_medidad($unidad,$Compania, $cantidaddes))).'</td>';
									$html.='<td valign="bottom">'.trim(utf8_encode($ardobs)).'</td>';
									
								}
								else
								{
									$html.='<td valign="top" >'.trim(utf8_encode($descripcion)).'<strong>('.$aarcod.')</strong></td>								
											<td valign="top" align="right">'.$cantidad.'&nbsp;&nbsp;</td>
											<td valign="top" align="right">'.$cantidaddes.'&nbsp;&nbsp;&nbsp;</td>';
									$html.='<td valign="top">'.trim(utf8_encode(unidad_medidad($unidad,$Compania, $cantidaddes))).'</td>';
									$html.='<td valign="bottom" align="center">________</td>';
								}
						$html.='
							</tr>';
                 
				} 
				
$html.='	</table></td></tr>	
			<tr>
				<td colspan="6"><hr /></td>
			</tr>
			</tbody>
		<tbody class="obj">
		<tr>
			<td align="right" valign="top"><strong>Observaciones:</strong></td>
			<td colspan="5">'.trim(utf8_encode($arqobs)).'<br /></td>
		</tr>
		<tr>
			<td colspan="6"><hr /></td>
		</tr>
			
		 <tr>
			 <td align="center" colspan="2">Despachado Por:</td>
			 <td align="center" colspan="1">&nbsp;</td>';
		
		if($arqsts=='06' || $arqsts=='07' || $arqsts=='08'){
			$html.='<td align="center" colspan="3">Recibido Por:</td>';
		}else{
			$html.='<td align="center" colspan="3">&nbsp;</td>';
		}
		
 		 $html.='
		 </tr> 
		 <tr>
		     <td align="center" colspan="2">'.trim(utf8_encode(usuario($aundesp))).'</td>
			 <td align="center" colspan="1">&nbsp;</td>
			 <td align="center" colspan="3">';
		
		if($arqsts=='06' || $arqsts=='07' || $arqsts=='08'){	
			$html.=''.trim(utf8_encode(usuario($aunrecp))).'';
		}else{
			$html.='&nbsp;';
		}
		
		$html.='
			</td>
		 </tr>
		 <tr>
		     <td align="center" colspan="2">________________</td>
			 <td align="center" colspan="1">&nbsp;</td>';
			 
		if($arqsts=='06' || $arqsts=='07' || $arqsts=='08'){
			$html.='<td align="center" colspan="3">________________</td>';
		}else{
			$html.='<td align="center" colspan="3">&nbsp;</td>';
		}
		
		$html.='</tr>';
		
		 if($arqsts=='05')
		 {
			$html.=' 
			<tr>
				 <td align="center" colspan="2"><br /><br /><br /><br />PCP Meditron C.A.:</td>
				 <td align="center" colspan="1">&nbsp;</td>
				 <td align="center" colspan="3">&nbsp;</td>
			 </tr>
			 <tr>
				 <td align="center" colspan="2">
					<table width="100%" cellpadding="3">
						<tr><td width="30%">Nombre:</td><td>_______________</td></tr>
						<tr><td>C.I.:</td><td>_______________</td></tr>
						<tr><td>Firma:</td><td>_______________</td></tr>
					</table>
				 </td>
				 <td align="center" colspan="1">&nbsp;</td>
				 <td align="center" colspan="3">&nbsp;</td>
			 </tr>';
		 }

$html.='
		</tbody>
    </table>';


  //echo $html;
$tbl = <<<EOD
	$html
EOD;

	$pdf->writeHTML($tbl, true, false, false, false, '');
	$pdf->Output('Despacho_Pedido_'.$arqnro.'.pdf', 'I');
	
?>