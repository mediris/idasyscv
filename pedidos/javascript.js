//Requerimientos
function agregar() {
	document.agregarform.arqobs.value="";
}

function agregarvalidar(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	
	document.body.style.cursor="wait";
	var indice1 = document.agregarform.arqtip.selectedIndex;
	var val1 = document.agregarform.arqtip.options[indice1].value;
	//alert(val1);
	
	capturararticulo(val1);

	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "pedidoagregarvalidarphp.php",
			data: $("#agregarform").serialize(),
			success: function(datos){//alert(datos);
				vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo){
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';		
		}
		if(vdata[i].ARQNRO){
			document.agregarform.arqnro.value=vdata[i].ARQNRO;
			document.solicitudes.arqnro.value=vdata[i].ARQNRO;
		}
	}
	
	if(sierror=='N'){
	document.body.style.cursor="default";
		if(nodetalle=="S"){
			finalizarreq();}
		else{
    		var indice1 = document.agregarform.arqtip.selectedIndex;
			
    		var texto1 = document.agregarform.arqtip.options[indice1].text;
			
			$("#agregarc").css("display", "none");
			$("#reqst06fp").css("display", "block");
			document.getElementById("ord1").innerHTML=texto1;
			document.getElementById("ord3").innerHTML=document.agregarform.arqobs.value;
			document.getElementById("aarcod").focus();
			$("#ocultarform").css("display", "none");}//fin del else
	}
}//fin del primer if

function choiceart(targ,selObj,restore,e){ //v3.0
	dat = selObj.options[selObj.selectedIndex].value;
	var datos = dat.split("@");
	//document.getElementById("aarumb").value=datos[1];
	combo_uni(datos[0], datos[1]);
	activaimagengrabar(1);
}

function capturararticulo(code)
{
	$.get ("buscararticulo.php", { code: code },function(resultado)
		{
			// alert(resultado);
			if(resultado == false)
			{
				alert("No existe ningun articulo para tipo de requisición");
				document.getElementById("aarcod").value=0;
				$("#aarcod").attr("disabled",true);
			}
			else
			{
				$("#aarcod").attr("disabled",false);
				document.getElementById("aarcod").options.length=1;
				$('#aarcod').append(resultado);
			}
		}
	);
}

function cargararticulo(code, alm)
{
	
	$.get ("buscararticulodespacho.php", { code: code , alm: alm  },function(resultado)
		{
			// alert(resultado);
			if(resultado == false)
			{
				alert("No existe ningun articulo para tipo de requisición");
				document.getElementById("aarcod").value=0;
				$("#aarcod").attr("disabled",true);
			}
			else
			{
				$("#aarcod").attr("disabled",false);
				document.getElementById("aarcod").options.length=1;
				$('#aarcod').append(resultado);
			}
		}
	);
}

function finalizarreq(){
	$("#agregaraftsav").hide(1000);		
	parent.location.reload();
	parent.Shadowbox.close();
}	
function finalizarreq_art_ext(){
	//alert($('#arqnro').val());
	guardardespacho2($('#arqnro').val());
	$("#agregaraftsav").hide(1000);		
	parent.location.reload();
	parent.Shadowbox.close();
}	

function enviarpedido(nrord){
	if (confirm('Seguro que desea enviar a aprobaci\u00f3n el pedido ' + nrord + '?'))
	{
		var param = [];
		param['arqnro']=nrord;
		//ejecutasqld("enviarrequerimiento.php",param);
		ejecutasqlp("enviarrequerimiento.php",param);
		
		for(i in gdata){
			if(gdata[i].EJEC == 1)//bien
			{
				var param = [];
				param['arqnro']=nrord;
				
				var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
				OpenWindow = window.open("correo.php?&arqnro="+nrord, "enviodecorreo", windowprops);
		
				//ejecutasqla("correo.php",param);
		
				$("#agregaraftsav").hide(1000);		
				parent.location.reload();
				parent.Shadowbox.close();
			}else if(gdata[i].EJEC == 0)//mal
			{
				alert("No se puede enviar aprobaci\u00f3n del Pedido \n Se debe ingresar Detalle.");
			}
		}
	}	
}

function activaimagengrabar(num){ 
	document.getElementById("aslcan").focus();
	if ((document.getElementById("aslcan").value !==0) && (document.solicitudes.aarcod.selectedIndex!=='')) {
		document.getElementById("imagengrabar").style.display='block';}
	if ((document.getElementById("aslcan").value == 0) || (document.solicitudes.aarcod.selectedIndex=='')) {
		document.getElementById("imagengrabar").style.display='none'}
}

function vertransaccion(){
	var code = $("#atocod").val(); 
	//alert(code);
	var param = [];
	param['atocod']=code;
	ejecutasqlp("buscarinventariotransaccion.php",param);
	for(i in gdata){
		document.agregarform.atrcod.value=gdata[i].ATRCOD;
		//var str=document.requerimagregarform.atrcod.value;
		//alert(str.length);
	};
}

function editar(nrord) {
			var param = [];
			param['arqnro']=nrord;
			ejecutasqlp("informacionphp.php",param);
			for(i in gdata){
				document.agregarform.arqobs.value=gdata[i].ARQOBS;
				document.getElementById("nroreq").innerHTML=gdata[i].ARQNRO;
				document.agregarform.arqnro.value=gdata[i].ARQNRO;
				document.agregarform.arqdes.value=gdata[i].ATQDES;
				document.agregarform.arqtip.value=gdata[i].ARQTIP;
				if (gdata[i].AARCOD!='') {
	
					var value4 = gdata[i].AARCOD+'@'+gdata[i].AARUMB;
					var texto4 = gdata[i].AARDES;
					var cantidad = gdata[i].ARDCAN;
					//var texto5 = gdata[i].AARUMB;
					var texto5 = gdata[i].AUMDES;
					cadena = "<tr>";
					//cadena = cadena + "<td>" + $("#adpcod2").val() + "</td>";
					cadena = cadena + "<td>" + texto4 + "</td>";
					cadena = cadena + "<td ><div align='right'>" + cantidad + "</div></td>";
					cadena = cadena + "<td>" + texto5 + "</td>";
					//cadena = cadena + "<td>" + $("#atrfac").val() + "</td>";
					//cadena = cadena + "<td>" + $("#aslcos").val() + "</td>";
					
					cadena = cadena + "<td><a class='elimina' a href='javascript:fn_dar_eliminar(\"" + value4 + "\");'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
				  
					cadena =  cadena + "</tr>";
					$("#grilla").append(cadena);
	
				};
			};
}
function editarextr(nrord) {
			var param = [];
			param['arqnro']=nrord;
			ejecutasqlp("informacionextraphp.php",param);
			for(i in gdata){
				
				if(gdata[i].AARCOD) {				
					if (gdata[i].AARCOD.trim()!='') {
						var value4 = gdata[i].AARCOD+'@'+gdata[i].AARUMR;
						var texto4 = gdata[i].AARDES;
						var cantidad = gdata[i].ARDCAN;
						//var texto5 = gdata[i].AARUMB;
						var texto5 = gdata[i].AUMDES;
						cadena = "<tr>";
						//cadena = cadena + "<td>" + $("#adpcod2").val() + "</td>";
						cadena = cadena + "<td>" + texto4 + "</td>";
						cadena = cadena + "<td ><div align='right'>" + cantidad + "</div></td>";
						cadena = cadena + "<td>" + texto5 + "</td>";
						//cadena = cadena + "<td>" + $("#atrfac").val() + "</td>";
						//cadena = cadena + "<td>" + $("#aslcos").val() + "</td>";
						
						cadena = cadena + "<td><a class='elimina' a href='javascript:fn_dar_eliminar(\"" + value4 + "\",this);'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
					  
						cadena =  cadena + "</tr>";
						$("#grilla").append(cadena);
		
					};
				};
			};
}

function editarcerrar() {
	document.getElementById('agregardiv').style.display='none';
	document.getElementById('fade').style.display='none';
}

function editarvalidar(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	
	document.body.style.cursor="wait";

	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "pedidoeditarvalidarphp.php",
			data: $("#agregarform").serialize(),
			success: function(datos){//alert(datos);
				vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
		document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
		sierror='S';		}
	}
	
	if(sierror=='N'){
	document.body.style.cursor="default";
	if(nodetalle=="S"){
		finalizarreq();}
	else{
		var texto1 = document.agregarform.arqdes.value;
		var val1 = document.agregarform.arqtip.value;
		capturararticulo(val1);

		$("#reqst06fp").css("display", "block");
		document.getElementById("ord1").innerHTML=texto1;
		document.getElementById("ord3").innerHTML=document.agregarform.arqobs.value;
		document.solicitudes.arqnro.value=document.agregarform.arqnro.value;
		document.getElementById("aarcod").focus();
		$("#ocultarform").css("display", "none");}//fin del else
	}
}//fin del primer if

function eliminar(nrord) {
	if (confirm('Seguro que desea borrar el pedido ' + nrord + '?')){
		var param = [];
		param['arqnro']=nrord;
		ejecutasqld("procesarrequerimeliminar.php",param);
		location.reload();
	}
}

//aprobar 
function aprobar(nrord) {
	if (confirm('Seguro que desea aprobar el pedido ' + nrord + '?'))
	{
		var param = [];
		param['arqnro']=nrord;
		ejecutasqld("aprobarrequerimiento.php",param);
		var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
		OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);
		location.reload();
	}
}

//aprobar 
function aprobare(nrord) {
	var param = [];
	param['arqnro']=nrord;
	ejecutasqld("aprobarrequerimiento.php",param);
	var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
	OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);
}

//Despachar , rechazar orden de servicio pendiente
function procesardespacho(nrord,array) {
	if (confirm('Una vez despachado el requerimiento ' + nrord + ' no se podr\u00e1  hacer modificaciones. Desea continuar?')){
		var cant = 0;
		for(var i=0; i<array.length; i++){
			var valor = document.getElementById(array[i]).value.replace(' ', '');
			//alert("**"+valor+"**");
			if(valor == ""){
				cant++;
			}
		}
		//alert(cant);
		
		if(cant==0){
			var param = [];
			guardardespacho(nrord);
			param['arqnro']=nrord;
			ejecutasqld("despacharrequerimiento.php",param);
			
			ejecutasqld("correo_art_ext.php",param);
			
			//var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=400,height=200";
			//OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);
			
			detalleimprimirpdf(nrord);
			
			window.opener.document.location.reload()
			window.close();
		}else{
			alert('La Cantidad a Despachar deben tener valor. (0 ~ *)');
		}		
	}
}


function recibirdespacho(nrord) {
if (confirm('Desea recibir el requerimiento ' + nrord + ' ?'))
{
		var param = [];
			$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "recibirrequerimiento.php",
        data: $("#despachoform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
});						

			
			
			//var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=400,height=200";
//OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);
			detalleimprimirpdf(nrord);

			window.opener.document.location.reload()
			window.close();
}
}


function guardardespacho(nrord) {
		
		var param = [];
			$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "guardarrequerimiento.php",
        data: $("#despachoform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
});	
			
			//var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=400,height=200";
//OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);

location.reload();
}

function guardardespacho2(nrord) {
		
		var param = [];
			$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "guardarrequerimiento.php",
        data: parent.$("#despachoform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
});	
			
			//var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=400,height=200";
//OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);

location.reload();
}


//Requerimiento grabar en is05fp

			function fn_cantidad(){
				cantidad = $("#grilla tbody").find("tr").length;
				$("#span_cantidad").html(cantidad);
			};
            
            function grabardetallereq(){
				var indice4 = document.solicitudes.aarcod.selectedIndex;
    			var texto4 = document.solicitudes.aarcod.options[indice4].text;
				var value4 = document.solicitudes.aarcod.options[indice4].value;
    			//var texto5 = document.solicitudes.aarumb.value;
				var indice5 = document.solicitudes.aarumb.selectedIndex;
    			var texto5 = document.solicitudes.aarumb.options[indice5].text;
				var indice5 = document.solicitudes.aarumb.selectedIndex;
                cadena = "<tr>";
                //cadena = cadena + "<td>" + $("#adpcod2").val() + "</td>";
				cadena = cadena + "<td>" + texto4 + "</td>";
                cadena = cadena + "<td> <div align='right'>" + $("#aslcan").val() + "</div></td>";
                cadena = cadena + "<td>" + texto5 + "</td>";
				//cadena = cadena + "<td>" + $("#atrfac").val() + "</td>";
				//cadena = cadena + "<td>" + $("#aslcos").val() + "</td>";
				
                cadena = cadena + "<td><a class='elimina' a href='javascript:fn_dar_eliminar(\"" + value4 + "\",this);'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
              
				cadena =  cadena + "</tr>";
				
				$.ajax({
					async: false,
					type: "POST",
					dataType: "JSON",
					url: "guardar_is05fp.php",
					data: $("#solicitudes").serialize(),
					success: function(datos){
						//alert(datos);
						vdata = parseJSONx(datos);
					}
				});			
                sierror='N';
				for(i in vdata)	{
					if(vdata[i].campo) 	{
						//document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
						sierror='S';
					}
				}
												
				if (sierror=='S')	{
					alert("Producto ya solicitado en el pedido")
				}else{
					$("#grilla").append(cadena);
					//alert("Articulo agregado");
				}
				document.getElementById("aarcod").value="";
				document.getElementById("aslcan").value="";
				document.getElementById("aarumb").value="";
				combo_uni(document.getElementById("aarcod").value, document.getElementById("aarumb").value);
				activaimagengrabar();
				document.getElementById("aarcod").focus();
            };
            
            function fn_dar_eliminar(cod){
                $("a.elimina").click(function(){
					hcd = document.solicitudes.arqnro.value;
                    id = $(this).parents("tr").find("td").eq(0).html();
                    respuesta = confirm("Desea eliminar el Articulo: " + id);
                    if (respuesta){//alert($(this));
                        //$(this).parents("tr").fadeOut("normal", function(){
									
                            $(this).parents("#grilla tr").remove();
                            
							var param = [];
							param['arqnro']=hcd;
							param['aarcod']=cod;
							$.ajax({
								async: false,
								type: "POST",
								dataType: "JSON",
								url: "eliminar_is05fp.php",
								data: convertToJson(param),
								success: function(datos){//alert(datos);
									eliminardetalleextra(cod);
									alert("Articulo " + id + " eliminado")
								}							
							});
                    }
					
                });
            };
			
			function grabardetalleextra(hcd,cod,can,umb){
				
				var param = [];
				param['arqnro']=hcd;
				param['aarcod']=cod;
				param['aslcan']=can;
				param['aarumb']=umb;
				$.ajax({
					async: false,
					type: "POST",
					dataType: "JSON",
					url: "guardar_is18fp.php",
					//data: $("#solicitudes").serialize(),
					data: convertToJson(param),
					success: function(datos){//alert(datos);
						vdata = parseJSONx(datos);
					}							
				});	
						
                sierror='N';
				for(i in vdata)	{
					if(vdata[i].campo) 	{
						//document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
						sierror='S';
					}
				}
												
				if (sierror=='S')	{
					alert("se produjo un error");
				}else{
					//alert("Articulo agregado");
				}
            };
			
			function eliminardetalleextra(cod){
				hcd = document.solicitudes.arqnro.value;
				var param = [];
				param['arqnro']=hcd;
				param['aarcod']=cod;
				$.ajax({
					async: false,
					type: "POST",
					dataType: "JSON",
					url: "eliminar_is18fp.php",
					data: convertToJson(param),
					success: function(datos){
						//alert(datos);
						//alert("Articulo eliminado");
					}							
				});
            };
			
			function agregardetalleextra()
			{
				var hcd = $( '#arqnro' ).val();
				var cod = $( '#aarcod' ).val();
				var can = $( '#aslcan' ).val();
				var umb = $( '#aarumb' ).val();
				//grabardetallereq();
				/**/
				var indice4 = document.solicitudes.aarcod.selectedIndex;
    			var texto4 = document.solicitudes.aarcod.options[indice4].text;
				var value4 = document.solicitudes.aarcod.options[indice4].value;
    			//var texto5 = document.solicitudes.aarumb.value;
				var indice5 = document.solicitudes.aarumb.selectedIndex;
    			var texto5 = document.solicitudes.aarumb.options[indice5].text;
				var indice5 = document.solicitudes.aarumb.selectedIndex;
                cadena = "<tr>";
                //cadena = cadena + "<td>" + $("#adpcod2").val() + "</td>";
				cadena = cadena + "<td>" + texto4 + "</td>";
                cadena = cadena + "<td> <div align='right'>" + $("#aslcan").val() + "</div></td>";
                cadena = cadena + "<td>" + texto5 + "</td>";
				//cadena = cadena + "<td>" + $("#atrfac").val() + "</td>";
				//cadena = cadena + "<td>" + $("#aslcos").val() + "</td>";
				
                cadena = cadena + "<td><a class='elimina' a href='javascript:fn_dar_eliminar(\"" + value4 + "\",this);'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
              
				cadena =  cadena + "</tr>";
				
				$.ajax({
					async: false,
					type: "POST",
					dataType: "JSON",
					url: "guardar_is05fp.php",
					data: $("#solicitudes").serialize(),
					success: function(datos){
						//alert(datos);
						vdata = parseJSONx(datos);
					}
				});			
                sierror='N';
				for(i in vdata)	{
					if(vdata[i].campo) 	{
						//document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
						sierror='S';
					}
				}
												
				if (sierror=='S')	{
					alert("Producto ya solicitado en el pedido")
				}else{
					$("#grilla").append(cadena);
					grabardetalleextra(hcd,cod,can,umb);
					//alert("Articulo agregado");
				}
				document.getElementById("aarcod").value="";
				document.getElementById("aslcan").value="";
				document.getElementById("aarumb").value="";
				combo_uni(document.getElementById("aarcod").value, document.getElementById("aarumb").value);
				activaimagengrabar();
				document.getElementById("aarcod").focus();
				/**/
				
				//grabardetalleextra(hcd,cod,can,umb);
				
			};
			
function requerimverdetalle(num) {
	self.name = "main"; // names current window as "main"
	
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=950,height=800";
	
	var ventana="requerimverdetalle.php?&num="+num;
	
	window.open(ventana,"", windowprops); // opens remote control

}

function requerimdespachar(num) {
	self.name = "main"; // names current window as "main"
	
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=950,height=800";
	
	var ventana="requerimdespachardetalle.php?&num="+num;
	
	window.open(ventana,"", windowprops); // opens remote control

}
function requerimrecibir(num, sts) {
	self.name = "main"; // names current window as "main"
	
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=950,height=800";
	if (sts=='05')	{var ventana="requerimrecibirdetalle.php?&num="+num;}
	else			{var ventana="requerimrecibirdetalle2.php?&num="+num;}
	
	
	window.open(ventana,"", windowprops); // opens remote control

}

function esenter(e) {
	var tecla;
	if (window.event) {tecla = window.event.keyCode;}
	else if (e) tecla = e.which;
	else return false;
	if (tecla == 13)   	{
	if ((document.getElementById("aslcan").value !=0) && (document.solicitudes.aarcod.selectedIndex!='')) {
	grabardetallereq(); 	}}
	else   				{return false;  }

}
function esenter1(e) {
	var tecla;
	if (window.event) {tecla = window.event.keyCode;}
	else 			if (e) {tecla = e.which;}
	else 			return false;
	if (tecla == 13)   	{return true; 	}
	else   				{return false;  }

}
function esenter2(e) {
	var tecla;
	if (window.event) {tecla = window.event.keyCode;}
	else 			if (e) {tecla = e.which;}
	else 			return false;
	//lert(tecla);
	if (tecla == 13)   	{return false; 	}
	else   				{return true;  }

}
function nada() {
}

function validarcantrec(candes, observ,cantrec) {alert("Debe llenar el campo de observaci\u00f3n, colocando motivo");
        if(cantrec.value != candes)	{	document.getElementById(observ).disabled=false;
										document.getElementById(observ).focus();		}								
		else 						{	document.getElementById(observ).disabled=true;	}
}
function validarobserv(observ) {
        if (observ.value=="")  {		observ.style.border = "1px solid #F00";
										alert("Se debe colocar una observaci\u00f3 n v\u00e1lida");                               
										observ.focus();}
}
function validarobservon(observ) {
        if (observ.value=="")  {		observ.style.border = "1px solid #F00";
										alert("Se debe colocar una observaci\u00f3 n v\u00e1lida");                               
										observ.focus();}
}

/*
 * @jDavila
 *13/04/2012
 */
function combo_uni(art, medi)
{
	var param = [];
	param['medi']=medi;
	param['art']=art;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarunimedida.php',
		beforeSend:function(){
			$('#div_aarnum').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#div_aarnum').html(html);
		}
	});
}

function detalleimprimirpdf(num)
{
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=950,height=800";
	//var ventana="exportarapdf.php?&num="+num;//ORIGINAL
	var ventana="exportarapdf2.php?&num="+num;//SGUNDARIO
	window.open(ventana,"", windowprops); // opens remote control
}

function pdf_pakinglist(num)
{
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=950,height=800";
	var ventana="exportapdf_despacho.php?&num="+num;//solo despacho 
	window.open(ventana,"", windowprops); // opens remote control
}

/*
 * @jdavila
 * 22/10/2013
 * 24/01/2014
 */
 
 function buscarexistencia(aarcod, aalcod, guardada)
 {
	 
	document.body.style.cursor="wait";
	//var aalcod = document.despachoform.aalcod.options[document.despachoform.aalcod.selectedIndex].value;
	var can = (guardada.value);//cantidad existente en el despacho
	
	var param = [];
	param['aalcod']=aalcod;
	//param['aarcod']=document.getElementById("aarcod").value;
	param['aarcod']=aarcod;
	ejecutasqlp("buscarexistenciaart.php",param);
	
	for(i in gdata)
	{
		if(gdata[i].EJEC != 0)//bien
		{
			var tem =0;
			if(!isNaN(can))tem = parseFloat(gdata[i].CANT);
			if(isNaN(can))tem = parseFloat(gdata[i].CANT)+parseFloat(can);
			document.getElementById('ardcdse['+aarcod+']').value=tem;
			document.body.style.cursor="default";
		}
		else if(gdata[i].EJEC == 0)//mal
		{
			document.getElementById('ardcdse['+aarcod+']').value=0;
			document.body.style.cursor="default";
		}
	}
 }
 
 function validarcantidadexist(canti, exist, guard)
 {
	/*cant  = NumFormat(Number(canti.value),'2', '10', '.', '');
	exist = NumFormat(Number(exist),'2', '10', '.', '')+cant;
	*/
	cant  = NumFormat(parseFloat(canti.value),'2', '10', '.', '');
	exist = NumFormat(parseFloat(exist),'2', '10', '.', '');
	guar  = NumFormat(parseFloat(guard),'2', '10', '.', '');

	if(isNaN(guar))exist = parseFloat(exist);
	if(!isNaN(guar))exist = parseFloat(exist)+parseFloat(guar);
	
	if(cant>0){
		if(cant>exist)
		{
			alert('La cantidad a despachas es mayor que \n la cantidad que posee en el almacen.');
			canti.value=0;
			//canti.select();
		}
	}else{
		alert('No se pueden despachar cantidades negativas o el valor no es valido.');
		canti.value=0;
		
	}
	 canti.focus();
	 canti.select();
 }